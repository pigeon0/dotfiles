# Table of contents

1. [Essentials](#Essentials)
    * [Main Essentials](#Main Essentials)
    * [Bug Fixes](#Bug Fixes)
    * [UI](#UI)
    * [Bug Fixes](#Bug Fixes)
2. [Textures](#Textures)
    * [Base](#Base)
    * [Clothing/Armor/Weapons](#Clothing/Armor/Weapons)
    * [Landscape](#Landscape)
    * [Miscellaneous Texture Replacer](#Miscellaneous Texture Replacer)
    * [Animals and Creatures texture replacer](#Animals and Creatures Texture replacer)
    * [Mod Retuxteures](#Mod Retextures)
3. [Environment](#Environment)
    * [Lighting](#Lighting)
    * [Weather](#Weather)
4. [World](#World)
    * [City Overhauls](#City Overhauls)
    * [Interiors](#Interiors)
    * [Creatures](#Creatures)
5. [NPC Overhaul](#NPC Overhaul)
    * [Body Textures and Meshes](#Body Textures and Meshes)
    * [Misc](#Misc)
    * [NPC Replacers](#NPC Replacers)
6. [Animations](#Animations)
    * [Basic](#Basic)
    * [Player](#Player)
    * [NPC](#NPC)
    * [Creatures](#Creatures)
    * [NSFW](#NSFW)
    * [Combat Animations](#Combat Animations)
7. [Gameplay](#Gameplay)
    * [Survival](#Survival)
    * [Lewd](#Lewd)
    * [Armor](#Armor)
    * [Weapons](#Weapons)
    * [Combat](#Combat)
    * [Other](#Other)
8. [Audio](#Audio)
    * [Creature](#Creature)
9. [Quest and World expansions](#Quest and World expansions)
    * [Small](#Small)
    * [Large](#Large)





# Essentials

## Main Essentials

### [SKSE](https://skse.silverlock.org/)

**NOTES**

* Available for AE/ SE and VR and LE
* Extracts everything to the Skyrim Directory

### [Adress Library](https://www.nexusmods.com/skyrimspecialedition/mods/32444?tab=description)

**NOTES**

* AE and SE version available
* Header file for AE and SE seperate

### [ConsoleUtil SSE](https://www.nexusmods.com/skyrimspecialedition/mods/24858)

**NOTES**

*  This mod enables papyrus scripts to execute console commands 


### [Jcontaners SE](https://www.nexusmods.com/skyrimspecialedition/mods/16495)

**NOTES**

* Requires Adress Library and SKSE
* VErsoins for AE/SE


### [Papyrus Util SE](https://www.nexusmods.com/skyrimspecialedition/mods/13048)

**NOTES**

* Install through mod manager
* Versions for AE/SE/VR

### [Racemenu](https://www.nexusmods.com/skyrimspecialedition/mods/19080)

**NOTES** 

* Install anniversery edition one

### [NL_MCM - A Modular MCM Framework](https://www.nexusmods.com/skyrimspecialedition/mods/49127)

**NOTES**

* Preferably overwrite the nl_mcm files in any other mods that redistribute the scripts.
* Dowload versions seperated between users and authors

### [powerofthree's Tweaks](https://www.nexusmods.com/skyrimspecialedition/mods/51073)

**NOTES**

* AE version and SE version available

### [powerofthree's Papyrus Extender](https://www.nexusmods.com/skyrimspecialedition/mods/22854)
### [Spell Perk Item Distributor(SPID)](https://www.nexusmods.com/skyrimspecialedition/mods/36869?tab=files)
**NOTES**

* AE/SE versoins available


**NOTES** 

* Requires VC++ 2015/17/19

**FOMOD**

* Options for AE and SE

### [Fuz Ro D'oh](https://www.nexusmods.com/skyrimspecialedition/mods/15109?tab=files)

## UI

[SkyUI](https://www.nexusmods.com/skyrimspecialedition/mods/12604)

**NOTES**

* Only requires SKSE64

[SkyUI Weapons Pack](https://www.nexusmods.com/skyrimspecialedition/mods/37231)


### [UI Extensions](https://www.nexusmods.com/skyrimspecialedition/mods/17561)


### [iWant Widgets](https://www.nexusmods.com/skyrimspecialedition/mods/36457)

**FOMOD**

* Skip all and click install

### [Easy Wheel Menu II](https://www.nexusmods.com/skyrimspecialedition/mods/59799)

**NOTES**

* Requires SKSE64, SkyUI, PapyrusUtil SE
* Native support for
- CBBE 3BA 
- AddItemMenu
- Bathing in Skyrim
- Campfire
- Dirt and Blood
- Extensible Follower Framework
- Facelight
- FlowerGirl
- Frostfall
- Hearthfire Multiple Adoption
- Hunterborn
- Immersive Horses
- iNeed
- Last Seed
- Nether's Follower Framework
- Vilja Follower
* Mods developed for the original should work with this

**PATCHES**

* [Stay in Shape](https://www.nexusmods.com/skyrimspecialedition/mods/63705)
* [Dovahkiin Relaxes too](https://www.nexusmods.com/skyrimspecialedition/mods/61298)
* [Bath Followers](https://www.nexusmods.com/skyrimspecialedition/mods/65318)

**FOMOD**

* Option for Sofia Follower
* Try my ouytfit

### [Dialouge Interface Reshaped](https://www.nexusmods.com/skyrimspecialedition/mods/46546)

**NOTES**

* Compatible with everything
* Not compatible with Better Dialogue controls
* E22C Dialogue Menu
* Minimalist Dialouge interface


## Bug Fixes

### [USSEP](https://www.nexusmods.com/skyrimspecialedition/mods/266)

### [Purist's Vanilla Patch](https://www.nexusmods.com/skyrimspecialedition/mods/32371?tab=description)
**NOTES**

* Recommended to load somewhere in the middle of the load order

### [Redbelly Mine VERY SERIOUS LORE Fixes (Ebony Ore Restored)](https://www.nexusmods.com/skyrimspecialedition/mods/42380)
**NOTES**

* Just make sure this loads after USSEP

### [Vanilla Plus Writing Purity Patch](https://www.nexusmods.com/skyrimspecialedition/mods/51232)
**PATCHES**

* Legacy of the Dragonborn and Finding Velehk Sain Combo Patch
* Legacy of the Dragonborn and The Choice is Yours Combo Patch
* Legacy of the Drgaonborn and Wintersun Combo patch
* Legacacy of the Dragonborn Patch

### [Unofficial Creation Club content patch](https://www.nexusmods.com/skyrimspecialedition/mods/18975?tab=files)


### [MFG Fixes](https://www.nexusmods.com/skyrimspecialedition/mods/11669)

**NOTES**

* Versions for AE/SE/VR

### [SkyUI - Flashing Savegames fix](https://www.nexusmods.com/skyrimspecialedition/mods/20406?tab=description)
### [Bug Fixes SSE](https://www.nexusmods.com/skyrimspecialedition/mods/33261)



# Textures

## Base

### [Cleaned Skyrim SE Textures](https://www.nexusmods.com/skyrimspecialedition/mods/38775)

**NOTES**: 

* All Skyrim SE textures cleaned of compression artifacts, removed bad bicubic upscaling, and other random fixes. Improves performance too.
* Put this after USSEP:
* Downlad Kart_CSSET_Overwrite_LATEST
* Then download KART_CSSET_snow_patch
       

### [Skyrim Special Edition Upscaled Textures (SSEUT)](https://www.nexusmods.com/skyrimspecialedition/mods/34560?tab=files)

**NOTES**:

* Base Upscaled texture for everything

### [SMIM](https://www.nexusmods.com/skyrimspecialedition/mods/659)

**NOTES**:

* No requirements needed
* Base mesh mod

### [Solstheim Objects SMIM's](https://www.nexusmods.com/skyrimspecialedition/mods/53779?tab=files&file_id=221515)

**NOTES**: 

**FOMOD**: 

* Choose to add ENB Particle lights
       
       
### [Ruins Clutter Improved](https://www.nexusmods.com/skyrimspecialedition/mods/5870)

**NOTES**: 

* Included SMIM textures
* If you are using ELFX, you will have to overwrite some meshes
* Install everything in FOMOD


### [Skyrim Realistic Overhaul](https://www.moddb.com/mods/skyrim-realistic-overhaul)(NOT USING)

**NOTES**:

* Base texture mod

### [Skyrim 2018](https://www.nexusmods.com/skyrimspecialedition/mods/20146?tab=description)(NOT USING)

**NOTES**: 

* Base texture mod that overwrites RSO

**FOMOD**: 

* Options - Landscape, Architecture, Armor
* Renthal's chicken - yes
* Surprise(4k retexture of cloaks) - no

**Patches in FOMOD**:

* ELFX
* Luxury Suite
* Ciris Outfit
* Kaer Mohren Armor

### [Skyrim 2019](https://www.nexusmods.com/skyrimspecialedition/mods/23283?tab=files&file_id=163442)(NOT USING)

**NOTES**: 

* Overwrite Skyrim 2018 with this


### [Skyrim 2020](https://www.nexusmods.com/skyrimspecialedition/mods/2347?tab=description)(NOT USING)

**NOTES**: 

* Overwrite Skyrim 2019 with this
* Download 2k Textures
* Download Whiterun wall alternative textures
* [Parallex](https://www.nexusmods.com/skyrimspecialedition/mods/54860?tab=description) here


**Patches**: 

* Blended roads retuxture

### [NOBLE SKYRIM](https://www.nexusmods.com/skyrimspecialedition/mods/21423)

**NOTES**

* 4k/2k versions available
* Performance edition available
* Don't forget to download the update file

**PATCHES**

* SMIM

## Clothing/Armor/Weapons

### [Improved Closed Faced Helmets](https://www.nexusmods.com/skyrimspecialedition/mods/824)

**NOTES**

* NOT compatible with any mod that modifies the mesh files or the stats of the subjected helmets.
* The mod with priority will overwrite the other one's changes. Therefore, if you see headless NPCs check for mod confilcts and adjust loading order.

**PATCHES***

* [Aphophysis DPM](https://www.nexusmods.com/skyrimspecialedition/mods/18039)
* [WACCF](https://www.nexusmods.com/skyrimspecialedition/mods/21747)
* [Guard Armor Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/38903)
* [Dragonborn DLC](https://www.nexusmods.com/skyrimspecialedition/mods/14333)
* [Dragon Priests Mask of Immersion](https://www.nexusmods.com/skyrimspecialedition/mods/15047)
* [Carved Brink](https://www.nexusmods.com/skyrimspecialedition/mods/24647)
* [FOMOD Patch Compendium that includes](https://www.nexusmods.com/skyrim/mods/15927?tab=files&file_id=1000170603&nmm=1);
- SkyRealism shiny(use this if using cathedral armory)
- Differently Ebony
- Silver Knight Armor
- Helgen Rebonr
- Warmonger Armory
- Dovahbit
- LoTD
- Death Mountain 2

### [Cathedral - Armory](https://www.nexusmods.com/skyrimspecialedition/mods/20199?tab=files&file_id=283406)

**NOTES**

* This mod uses cube maps that makes metal actually shine like metal, so if there is any mod that replaces vanilla meshes will not get this metal shine

**PATCHES**

* aMidianBorn Skyforge Weapons Add-On(Needs the plugin)
* Armor Mesh Fixes SE patch
* Armor Variant Expansion Patch
* CBBE BodySlide Patch
* Improved Closefaced Melmets patch
* Put this mod after any armor/weapon overhauls
* Let rustic clothing overwrite this mod
* [Believable weapons](https://www.nexusmods.com/skyrimspecialedition/mods/46199?tab=description)(may be outdated)


### [Rustic Clothing](https://www.nexusmods.com/skyrimspecialedition/mods/4703)

**NOTES**:

* Re texture of clothing 

### [aMidianBorn Book of Silence](https://www.nexusmods.com/skyrimspecialedition/mods/35382)

**NOTES**: 

* Armor/Weapon re texture that covers many weapon/armor but not all

### [Rustic Armors and Weapons](https://www.nexusmods.com/skyrimspecialedition/mods/19666)

**NOTES**: 

* Covers other stuff that aMidianBorn did not


### [Believable Weapons](https://www.nexusmods.com/skyrimspecialedition/mods/37737)

**NOTES**:

* Re-mesh of weapon to make weapons more realistic.
* Compatible with any re-textures
* Incompatible with re-meshes

**FOMOD**:

* Installtion type - full
* Refracting glass non tranparant
* Sword of Jyggalag fix

**Patches**:

* In FOMOD
           1. ALLGUD, DSR, EDS
           2. Frankly dawngard
	   3. Frankly Imperial
	   4. Refractive ebony
	   5. aMidianBornSkyforge weapon

### [LeanWolf's Better-Shaped Weapons SE](https://www.nexusmods.com/skyrimspecialedition/mods/2017)(NOT USING)

**NOTES**: 

**FOMOD**:

* Choose all
* Nighangle Blade Sheath
* Dawnbreaker Sheath ELF
* Keening Sheath
* Scimitar bling
* Keening with refraction
* Refractive glass weapons
* Stalhrim with refraction
* Dawnbreaker for ENB
* Default Dragonbone meshes(no glowy stuff)

**Patches**:

* In FOMOD;
            1. Frankfamily HD Imperial Armor and weapons(Overwrite this with LBSW)
            2. FrankFamily UltraHD Sliver Sword(Overwrite this with LBSW)
	    3. Runied Nord hero weapons(Overwrite this with LBSW)
	    4. GEMLING queen Jewelry extra feature with Dragonbone weapon not a patch
	    5. Dual Sheath Meshes


### [FrankFamily's RETEX mods](https://www.nexusmods.com/skyrimspecialedition/users/2531318?tab=user+files&BH=0)

**NOTES**: 

* To cover armor and weapon retex's previous mods did not
* Choose 2k textures for all
* Frankly HD Dragonbone and Dragonscale - Armor and Weapons
* Frankly HD Thieves Guild Armors(compatible with mesh replacers)
* Frankly HD Nightingale Armor and Weapons; 
                                           1. compatible with mods modifying the cowl's mesh
        				   2. Choose capeless and fingerless golves in FOMOD	
                                           3. Default textures
					   4. Choose the CBBE set for females
					   5. Leanwolfs Better-Shaped weapons and Dual Sheath Redux patch
					   6. Choose 2k in FOMOD
* Frankly HD Imperial Armor and Weapons:
                                        1. Is compatible with mesh replacers
                                        2. Download 2k version
				        3. Has patch for better shaped weapons
* Frankly HD Shrouded Armor:
                            1. Dont choose oblivion style in FOMOD
* Frankly HD Masqe of Clavicul Vile
* Frankly HD Dawngard Armor and Weapons:
                                        1. It's partially compatible with mesh replacers
				        2. CBBE version
					3. DSR Meshes
					    
### [Real Bows](https://www.nexusmods.com/skyrimspecialedition/mods/3144?tab=description)

**NOTES**: * Realistic bows
       * Install the alternate textures addon
    
**ALT Textures FOMOD**: * Choose all
			    
### [Quivers Redone](https://www.nexusmods.com/skyrimspecialedition/mods/65921)

**NOTES**: * Download the 2k version
       * Also the hotfix and merge


### [Soaking Wet - Character Wetness Effect](https://www.nexusmods.com/skyrimspecialedition/mods/68025)
**NOTES**

* Requires Adress library and SKSE64



## Landscape

### [Tamrielic Textures - Landscapes](https://www.nexusmods.com/skyrimspecialedition/mods/32973)(NOT USING)

**NOTES**: 

* Base Landscape mod

**Patches**:

* in FOMOD:
            1. Majestic Mountains
            2. Better Dynamic Snow, 

### [Septentrional Landscapes](https://www.nexusmods.com/skyrimspecialedition/mods/29842)

**NOTES**

* 8K, 2K and 4K versions available

### [Northern Shores](https://www.nexusmods.com/skyrimspecialedition/mods/27041)

**NOTES**

* 8K, 2K and 4k Versions available

### [Flully Snow](https://www.nexusmods.com/skyrimspecialedition/mods/8955)(NOT USING)

**NOTES**: 

* Snow mod

**Patches**: 

* Blended Roads
* 3d Trees
* Majestic Mountains
* iNeed

### [Hyperborean Snow](https://www.nexusmods.com/skyrimspecialedition/mods/29283?tab=images)

**NOTES**

* 2k, 4k and 8k versions available

### [Simplicity of Snow](https://www.nexusmods.com/skyrimspecialedition/mods/56235)

**NOTES**: 

* 0 worldspace edits
* 0 cell edits
* not compatible with any Riekling architecture retexture that changes the shape of the transparency
* Load ESP after anything that edits snow material shaders such as Majestic Mountains
* Patching instructions provided on the mod page
* Parallax meshes provided
* Put this after SMIM

**PATCHES**: 

* Beyond Skyrim Bruma patch
* Cities of the North
* Capital Windhelm	
* Blended Roads
* Unofficial Lux

### [Glacierslab](https://www.nexusmods.com/skyrimspecialedition/mods/24178)

**NOTES**

* Standard and Bumpy variants available

### [Frankly HD Markarth](https://www.nexusmods.com/skyrimspecialedition/mods/18050?tab=files&file_id=279170&nmm=1)

**NOTES**

* 4K and 2K versions available
* Missing texture update available
* Wood beams fix 
* Overwrite Underground and Noble Skyrim

**FOMOD**

* Options - **Exterior Markarth** / **Interiors and Dwemer Ruins**

### [Realistic Water Two](https://www.nexusmods.com/skyrimspecialedition/mods/2182)

**NOTES**: 

* Recommended for Users to patch themselves. A [tutorial](https://www.youtube.com/watch?v=YuQ0lJat_w0) is present.

**Patches**:

* Beyond Skyrim: Bruma
* Falskaar
* Open Cities 
* Wyrmstooth
* Beyond Reach 
* Saints an Seducers
    
**FOMOD**:

* ENB Rain - yes
* Water texture resolutions - none
* Smaller water drops - none
* alternate volcanic watercolor - none



### [Water for ENB](https://www.nexusmods.com/skyrimspecialedition/mods/37061?tab=description)

**NOTES**:

* GKB Waves is recommended by the author

**Compatibility**:

* Compatible with:
                  1. iNeed
                  2. Keep it Clean
		  3. 3. Realistic Needs and Diseases
		  4. 4. TDG's Wade in Water
* Works great with Depths of Skyrim 
	       
**FOMOD**: 

* Water Color - Yes
* iNeed support - no
* LOD water - default
* Water Texture Res - 2K
* Waterfalls and FX - 2k
    
**Patches**:

* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/50394?tab=files) for 
                  1. USSEP
* Patches in FOMOD:
                   1. Landscape fixes for grass mods
	           2. Lakeview Manor Avant Garden
	           3. Half Moon Creek
        	   4. Folkvangr
	           5. Flat World Map Framework
          	   6. Expanded Towns and Cities
         	   7. Darker Volcanic water
		   8. Clear Underwater
	           9. Clear Muddy Water
		   10. Clear interior water
		   11. Atlas Map Markers
			    
**Issues**:

* Water seams, usually happens when load order is not sorted right. If not use this [fix](https://www.nexusmods.com/skyrimspecialedition/mods/9087).
* [Blacksmith forge water fix](https://www.nexusmods.com/skyrimspecialedition/mods/1291), [xEdit script](https://www.nexusmods.com/skyrimspecialedition/mods/29758)
               
**Notes** 

* Make sure nothing overwrites this mods worldspace records.


### [Majestic Mountains](https://www.nexusmods.com/skyrimspecialedition/mods/11052)

**NOTES**

* Terrain LOD redone is suggested by the mod author
* Has DynDLod lod pack
* Use main version
* If using 'HD Lods' don't overwrite
* Overwriten the meshes with the meshes of water mods
* If in conflict with ELFX, use ELFX meshes
* If using ENB mesh fixes, overwrite it with Majestic Mountains
* this mod isn't compatible with other mountain retextures
* this mod is compatible with all major snow mods

**FOMOD**

* Snow Mountain Type - **ESL**/ESP
* Optionals - **Moss Rocks ESL Version**, Moss Rocks ESP version, Effect Meshes(Only use if not using RW2)
* Sun Direction - North/South/**None** 

### [Underground A dungeon texture overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/14365)

**NOTES**: 

* If third option is chosen for the imperial part; You need to select the Imperial default OR the Imperial darker exteriors together with the basic Imperial textures. Otherwise you wouldn't install the exterior parts of the Forts

### [Happy Little trees](https://www.nexusmods.com/skyrimspecialedition/mods/50961)

**FOMOD**:

* Trees - all trees
* Tree size - none
       
**Notes**: 

* Bilboard availabe at nexus page

### [Blended Roads](https://www.nexusmods.com/skyrimspecialedition/mods/8834)

**Notes**:

* Road texture mods with no alpha channel have no effect
* blending within one mesh is possible but between meshes not, this is nearly impossible in Skyrim
* he new collision can't be accurate because of avoiding hitching, character will stand some inches in the air (almost invisible)
* collision and decals like blood interact with each other, all tested decals worked fine (but it MAY be possible that some are blotchy, I haven't found some)
* Opitional file for medieval like bridges
       
**Compatibility**:

* Incompatilbe with Real Roads
* Combatible with SMIM bridges
* No issue with Fluffy Snow and Majestic Mountains
	       
**FOMOD**:

* Main Files - Really Blended Roads
* SMIM compatibility - yes

### [Blended Roads Redone](https://www.nexusmods.com/skyrimspecialedition/mods/26270)

**NOTES**

* Requires Blended Roads
* Textures and Meshes are seperate
* 2K, 4K and 8K versions available

### [3D Trees and plants](https://www.nexusmods.com/skyrimspecialedition/mods/12371?tab=description)

**NOTES**:

* Only the plants are to be used in from this mod
* Download and install [this](https://drive.google.com/file/d/1KD21xuRPvISGHvgaxQlQclCo6JZFQtkG/view) and enable this esp
* Overwrite tree/meshes and textures of this mod with Happy Little Trees, if asked
* Should be loaded before RW2
* Should be loaded before City overhaul mods 
* Should be loaded after any mods that add any trees or plants in cells
* Should be loaded after any mod that affect wind animations, although recommended to remove those mods completely

### [Mari's Flora]()

**NOTES**

* Overwrite RSO with this mod

### [Voloptious Grasses](https://www.nexusmods.com/skyrimspecialedition/mods/22437)(NOT USING)

**NOTES**:

* Don't overwrite this mod with any other grass mod
* Should be loaded late in the load order
* DynDLOD stuff should be loaded after this mod
* Overwrite Maris Flora with this mod

### [The Grass Your Mother Warned About](https://www.nexusmods.com/skyrimspecialedition/mods/53064)(NOT USING)

**NOTES**:

* This mod must be loaded under your landscape textures and above any mod that does land edits (water mods included
* load this mod after any mod that touches swordfern.dds and fallforestobject02.dds (Like Skyrim Flora Overhaul, Mari’s AIO and others)
* Landscape Fixes For Grass Mods should be loaded after this and its patch subsequently later

**Patches**:

* Landscape Fixes For Grass Mod
        
### [QW's Grass Patch - Veydosebrom Regions - Folkvangr - Cathedral - Depths of Skyrim](Not using)
**NOTES**

* Install Depths of Skyrim
* Don't ESL or Compathc DOS
* Install Folkvangr
* Install Veydosebrom Regions
* Install Cathedral 3D pines gass - Ful 3D coverages
* Than install this mod and overwrite any conflicts
* ESL-fy Folkvangr and Veydosebrom using xEdit
* LOOT Rules, Folkvangr > Load after > Depths of Skyrim, QW's Grass Patch > Load After > Majestic Mountains.esp
* Set iMinGrassSize=20


**Veydosebrom FOMOD**

* Fresh Install
* High Quality
* Dense Landscape
* Default


### [ENB Complex Grass - Patch for QW'r Grass patch](https://www.nexusmods.com/skyrimspecialedition/mods/67304?tab=files&file_id=282994&nmm=1)
**NOTES**

* ENB must be higher than 0.473
* Let this mod overwrite Cathedral 3D mountain flowers
* Disable this mod during LOD generation

### [Cathedral - 3D Tundra Cotton](https://www.nexusmods.com/skyrimspecialedition/mods/68068)

### [Cathedral - 3d Lavender](https://www.nexusmods.com/skyrimspecialedition/mods/68310?tab=files&file_id=284913&nmm=1)





## Miscellaneous Texture Replacer


### [Glorious Doors of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/32376)

**NOTES** 

* They are all compatible with Open Cities Skyrim 

**FOMOD**

* Texture Size for all doors - 4k
* Rotation fix and sliding solitude door - yes

### [Scrumptious Stew HD](https://www.nexusmods.com/skyrimspecialedition/mods/21794?tab=files)

**NOTES**:

* Hearty Meat and Vegetable stew

### [Stunning Statuies of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/3375?tab=files&file_id=9840)

**FOMOD**:

* Astonich Azura
* Terrific Talos
* Ysterical Yrsgamor
* Melancholic Meridia
* Weakenend Winterhold
* Clever Clavicus
* Buculic Boethiah
* Necrotic Namira
* Magnetic Mara
* Malefic Mehrunes
* Vehement Vaermina
* Spicy Skyforge
* Fancy Falmer
* Sweet Sovngarde
* Ultra HD or HD  meshes and 2k textures for all
* Black Meridia
* Malacath Stone
* ENB winterhold meshe
* Mehrunes Dagon Black
* Skyforge non-steel
* Falmer normal copper
* Dibella and Nocturnal not chosen because of weird plasic surgerly breasts

### [Dibella statue replacer](https://www.nexusmods.com/skyrimspecialedition/mods/46747)

**NOTES**: 

* Not compatible with any mod that replaces vanilla meshes and textures of the statue of dibella

### [Better Skyrim Statues](https://www.nexusmods.com/skyrimspecialedition/mods/36601?tab=description)

**NOTES**: 

* Download Ebony Nocturnal

### [Frankly HD Masque of Clavicus Vile](https://www.nexusmods.com/skyrimspecialedition/mods/28565)

**NOTES**: 

* Download 2k version

### [The streeets of whiterun in HD](https://www.nexusmods.com/skyrimspecialedition/mods/20396)(NOT USING)

**NOTES**:

* High quality whiterun streets texture replacer

### [MD's Farmhouse](https://www.nexusmods.com/skyrimspecialedition/mods/32160)

**NOTES**:

* Download the 2k version
* Download 4k door

### [JS Dragon Claws AE](https://www.nexusmods.com/skyrimspecialedition/mods/57038)

**NOTES**:

* Download the 2k version
* Overwrite other mods with this
* Incompatible with JS Draogon Claws SE - Patches
* Overwrite previous patch for LoDT
* Jade claw patch no longer needed
* Dragondlaw Display available

**PATCHES**

* In FOMOD
- Legacy of the Dragonborn + Display 
- Wyrmstooth 
- Wyrmstooth + LoTD
- Konahrik's Accoutrements
- Konahrik's Accoutrements + LoTD
- Helgen Reborn 
- Helgen Reborn + LoTD
- Skyrim Sewers
- Skyrim Sewers + LoTD


### [KD - Realistic fire places](https://www.nexusmods.com/skyrimspecialedition/mods/28877)

**FOMOD**

* Cut yellow wire
* Choose ESP version
* Choose 4k textures
* Between Fire glow, red glow and no glow, chose Fire glow
* Choose Complex particle light option
* Choose sweet spot as intensity

**PATCHES**

* Has an Embers HD patch in FOMOD

### [Inferno](https://www.nexusmods.com/skyrimspecialedition/mods/29316)

**NOTES**

* Works with SMIM, Unoffical Material fix, ENB particle patch, ENB light

**FOMOD**

* Choose Ultra resolution
* CHoose Brighter glow options
* Choose freesia flame color
* Choose new flame tile
* choose normal flame size
* choose plugin option

**PATCHES**

* FOMOD Patches 
				1. Smoking Torches and Candles in FOMOD 
				2. Ember HD in FOMOD
				3. KD - Realistic Fireplaces in FOMOD

### [Embers XD](https://www.nexusmods.com/skyrimspecialedition/mods/37085)

**NOTES**

* Lite version available
* No scripts
* Also has a fix for particle lights on forges and no torch particle light pathc not working when using smoking torches or mathy's medieval touch
* This mod uses particles for the flame effect so to make sure the flames and the rest of the effects are displayed correctly
* ENB settings;
[COMPLEXPARTICLELIGHTS]
EnableBigRange=true
* Skyrim prefs settings
[Particles]
iMaxDesired=1500


**FOMOD**

* Has optoins for inferno flames among different colors(dont use torch plugin addon or fire magick addon) - Choose inferno flames
* Choose hig quality particli lights
* Choose normal intensity
* Forges addon - yes
* Lava craters opiont - yes
* Select disable helgen glow
* Less flying embers option - yes
* Disabled red glow- no
* Reduced embers glow - no
* Reduced yellow glow - no

**PATHES**

* In FOMOD
          1. Lux Orbis
		  2. Breezehome TNF Beds Plus
		  3. Breezehome TNF Expanded
		  4. ClefJ's Karthwasted
		  5. ClefJ's Winterhold
		  6. EEK's Beautiful Whiterun + dawn of skyrim
		  7. EEK"s Beautiful whiterun + JK's skyrim
		  8. EEK"s Beautiful Whiterun + JK's Whiterun
		  0. EEK"S whiterun interiors
		  9. ELFX
		  10. ELFX interiors
		  11. JK's skyrim + LOS II
		  12. JK's skyrim + Los II + Capital windhelm
		  13. JK's Skyrim + Los II + capital windhelm + elfx exteriors
		  14. JK's skyrim + LOS II + ELFX interiors
		  15. LOS II
		  16. Mathy's medieval torch
		  17. Perseids Inns and Taverns
		  18. Riverwood Inn by nesbit
		  19. Reodrk's Dragonbridge
		  20. Skyrim 3d Blacksmith (Original)
		  21. Skyrim 3d Blacksmith (Withouit Logs)
		  22. Skyrim 3  cooking
		  23. Skyrim Farmhouse Inns
		  24. Tamriel Master lights - cities version
		  25. Tamriel Master lights - standard version

### [Water in Wells](https://www.nexusmods.com/skyrimspecialedition/mods/57268)

**NOTES**

* Needs limited patching
* Overwrite SMIM

**FOMOD**

* Parralax options for all

### [Animated forge water](https://www.nexusmods.com/skyrimspecialedition/mods/52322)

**NOTES**

* Overwrite SMIM and USSEP 

### [Stockades of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/43227?tab=files)

**NOTES**

* Compatible with SMIM

### [FYX - 3D Stockades - Walls and Gate](https://www.nexusmods.com/skyrimspecialedition/mods/66037)

**NOTES**: 

* Literally compatible with anything
* Has SOS and parallax download options
* Download the non-parallax version

### [FYX Smooth wells](https://www.nexusmods.com/skyrimspecialedition/mods/66715)

* 

### [Uniqe Skulls HD](https://www.nexusmods.com/skyrimspecialedition/mods/52073)

**NOTES** 

* Texture replacer for unique skulls in the game
* Download 2k version

**Patches**:

* ENB light
* Skyrim Unique Treasures
* WACCF
* Uniqe Treasures

**FOMOD**: 

* Main plugin option - esp
* Skullkeys enblight patch - yes
* LODT options - none
       
### [Overlooked Dungeon Objects Retextures](https://www.nexusmods.com/skyrimspecialedition/mods/66418)

**NOTES**: 

* Self Explanatory
* Choose all options	

### [High Poly Project](https://www.nexusmods.com/skyrimspecialedition/mods/12029?tab=description)

**NOTES**: 

* Load this mod after; 
                      1. SMIM
                      2. ELFX
		      3. Rustic Clutter Collection
		      4. Particle/Subsurface Mindflux patches
	    	      5. HD Photorealistic Ivy
	    	      6. Forgotten Retex Project
	
* Load this mod before Skyrim 3d Trees and EEK's Renthal Flora Collection
* Snowfix addon in FOMOD

**Patches**: 

*  In FOMOD 
            1. Firewood snowfix Campfire
            2. No snow under roof

### [Real Hearts 4K - 2K - 1K](https://www.nexusmods.com/skyrimspecialedition/mods/66546)

**NOTES**:

* Heart Retexture

### [Forgotten Retex Project](https://www.nexusmods.com/skyrimspecialedition/mods/7849)

**NOTES**: 

* Retextures common stuff that is usually not covered by other mods

### [Uniqe Hand made signs overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/21704)

**NOTES**: 

* Download 2k version

### [Elder Scroll HD - SE](https://www.nexusmods.com/skyrimspecialedition/mods/51082)

**NOTES**:

* Elder Scrolls replacer HD
* animated
* Choose the 4k version
       
**FOMOD**:

* ESP
* No mainmenu replacer
* cube mapfile from cathedral armory -no
* Elder Scroll chest - yes

**Patches**:

* LOTD
* Cunny's Improved Dwemer Glass Unofficial Material Fix
* Bards Rebonr Song

### [Elsopa's Skeleton Key](https://www.nexusmods.com/skyrimspecialedition/mods/21992)

**NOTES**: 

* Retexture for Skeeton Key

### [ElSopa HD - Small Compilation Pack SE](https://www.nexusmods.com/skyrimspecialedition/mods/29160?tab=files)

**NOTES**: 

* Compilation pack for Elsopas retextures

**FOMOD**: 

* Meadbarrel
* Grindstone
* Giant Mortar
* Burial Urns
* Briarheart red
* Bonewhawk Amulet
* Anvil
* Akatosh Amulet
* Striders and Netches
* Smelter

### [The Lovely Kettle](https://www.nexusmods.com/skyrimspecialedition/mods/36511?tab=files&file_id=142245)

**NOTES**: 

* Kettle retexture

**FOMOD**:

* Choose 2k diffuse
* Choose compressed 2k normals

### [ElSopa - Tankard HD](https://www.nexusmods.com/skyrimspecialedition/mods/43764?tab=files)

**NOTES**: 

* Download the 2k version
* Overwrite SMIM with this

### [HD-Keys Redone](https://www.nexusmods.com/skyrimspecialedition/mods/48209?tab=files&file_id=196463)

**NOTES**:

* Key retexture

### [Glorious Dummies SE](https://www.nexusmods.com/skyrimspecialedition/mods/20865?tab=files&file_id=70650)

**NOTES**:

* Dummy retexture

**FOMOD**:

* Install the 2k version

### [Hold Guard Shields](https://www.nexusmods.com/skyrimspecialedition/mods/26141)

**NOTES**: 

* High quality retexture of hold shields
* Download the 2k versoin

### [ElSopa - Papers HD SE](https://www.nexusmods.com/skyrimspecialedition/mods/33422)

**NOTES**: 

* Download the papers HD version

### [Book Covers of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/901?tab=files)

**NOTES**: 

* Download the desaturated version

### [Rustic Furniture](https://www.nexusmods.com/skyrimspecialedition/mods/17228)

**NOTES**:

* Download SE SMIM 2k
* Overwrite SMIM with this

### [CC's HQ Alduins Wall](https://www.nexusmods.com/skyrimspecialedition/mods/2638?tab=files&file_id=3816)

**NOTES**: 

* This is a simple tweak of the Texture and NormalMap of Alduins wall to make it a bit more detailed and less lumpy.


### [Renthal's workbench](https://www.nexusmods.com/skyrimspecialedition/mods/23164?tab=files&file_id=80982)
### [Gemling Queen Jewelry SE](https://www.nexusmods.com/skyrimspecialedition/mods/4294?tab=files&file_id=9441)

**NOTES**: 

* Replacers for most, if not all Jewellery in Skyrim
* Download the AIO esp
       
**FOMOD**:

* Main Modules - all
* DLC Addons - all
* Amulet textures - Gamwich textures 1024
* Ring Texture options - Gamwich Ring textures - combined 2k
* Mesh Pack Daungard - yes
       
**Patches**:

* Left hand rings

### [Rustic Amulets SE](https://www.nexusmods.com/skyrimspecialedition/mods/35485?tab=description)

**NOTES**:

* Additional Amulet Textures


### [Rustic Soulgem](https://www.nexusmods.com/skyrimspecialedition/mods/5785?tab=description)

**NOTES**: 

* Soulgem replacer that is animated and looks fucking cool
* Download the FOMOD version

**FOMOD**: 

* Textures - 2k
* Plugin type - unsorted + ESL

**Patches**: * GIST

### [Rustic Windows](https://www.nexusmods.com/skyrimspecialedition/mods/1937)

**NOTES**:

* Lore-friendly vanilla windows replacer
* Download 2k version

### [Detailed Rugs](https://www.nexusmods.com/skyrimspecialedition/mods/9030)

**NOTES**: 

* Lore friendly Rug replacer
* Download the non-clean rugs

### [Peltapalooza](https://www.nexusmods.com/skyrimspecialedition/mods/5442?tab=files)

**NOTES**:

* High quality pelt replacer
* Download the full version that includes bedroll

### [Rusting Cooking](https://www.nexusmods.com/skyrimspecialedition/mods/6142?tab=files&file_id=14122)

**NOTES**: 

* High quality cooking oven and stove replacer
* Has some food textures but will be replaced by another mod
* Overwrite this with hearty meat and vegetable stew

### [Rustic Maps](https://www.nexusmods.com/skyrimspecialedition/mods/42614?tab=files)

**NOTES**: 

* Download 4k-2k version

### [High quality food and ingredients SE](https://www.nexusmods.com/skyrimspecialedition/mods/10897?tab=files&file_id=30487)

**FOMOD**:

* Choose everything

### [Awesome potions simplified](https://www.nexusmods.com/skyrimspecialedition/mods/57607?tab=files)(NOT USING)

**NOTES**:

* Animated potions
* No drink visual fx

**Patches**: 

* ZUPA
* ALLGUD

### [Rustic Animated Potions](https://www.nexusmods.com/skyrimspecialedition/mods/2276)

**NOTES**

* Not compatible with anything that changes vanilla mesh presets apart from white phial


### [Elsopa HD - Smelters SE](https://www.nexusmods.com/skyrimspecialedition/mods/22524?tab=files)

**NOTES**:

* Smelter replacer
* Download the 2k version

### [JS Instruments of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/51959)

**NOTES**: 

* Download the 2k version

### [JS Shrines of the Divines](https://www.nexusmods.com/skyrimspecialedition/mods/33394)

**NOTES**:

* Texture replacer for shrines
* Download the 2k version

### [ElSopa - HD Grindstone Redone SE](https://www.nexusmods.com/skyrimspecialedition/mods/58149?tab=description)

**NOTES**: 

* Grindstone replacer
* Choose the 2k version

**NOTES**: 

* High poly remesh and retexture of the workbench

### [ElSopa - Campfire HD SE](https://www.nexusmods.com/skyrimspecialedition/mods/24511)

**NOTES**

* This mod should overwrite Campfire
* Dowload 2k versions for all

### [Dirt and Blood HD Retexture](https://www.nexusmods.com/skyrimspecialedition/mods/44162)

* Optional ESL-flagged plugin
* Compatible with EBT
* Compatible with JUst blood - choose no when asked to install optional plugin
* Overwrite whith Dirt and blood

**FOMOD**

* Resolution - **2K**/ 4k
* Blood Brightness - **Default**/ Brighter/ Darker
* Optional Plugin - **Yes**/ No

### [JS Common Cages SE](https://www.nexusmods.com/skyrimspecialedition/mods/68236)

**NOTES**

*  Has 4k/2k versions

### [Cloaks of Skyrim Retexture](https://www.nexusmods.com/skyrimspecialedition/mods/42558)

**NOTES**

* Overwrite Cloaks of Skyrim with this mod
* Download 2k versoin

### [Mead Retexture](https://www.nexusmods.com/skyrimspecialedition/mods/68150?tab=description)

**NOTES**

* 2k or 4k version available

### [JS Dwemer Ichor Barrels](https://www.nexusmods.com/skyrimspecialedition/mods/67162)

**NOTES**

* INcludes enb particli light
* Has both 2k and 4k versoins

### [JS Dwemer Artifacts SE](https://www.nexusmods.com/skyrimspecialedition/mods/67631)

**INFO**

This mod remakes dwemer dungeon clutter

**NOETS**

* Has 2k or 4k versions
* Hs ENB Particle light for Centurion Dynamo COre

### [JS Bloodstone Chalice](https://www.nexusmods.com/skyrimspecialedition/mods/66038)

Complete remake of the bloodstone chalice

**NOTES**

* Comes in 2k or 4k versions

### [JK's Purses and Septims](https://www.nexusmods.com/skyrimspecialedition/mods/37306)

Complete remake of septimes, coin purses, and coin piles

**FOMOD**

* Purse Textures - 4k / **2k** / 1k
* Coin Textures - 4k / **2k** / 1k(**Dirty** or clean)
* Coin Pile Textures - 4k / **2k** / 1k(**Dirty** or clean)
* Replacer Options - Vanilla OVerwrite / **Path Relocation**


### [JS Barenziah](https://www.nexusmods.com/skyrimspecialedition/mods/22990)

Retexture of barenziahs crown

**NOTES**

* 2k or 4k versions available

### [JS Dwemer Kitchenware SE](https://www.nexusmods.com/skyrimspecialedition/mods/67026)

Remake of Dwemer Kitchenware

**NOTES**

* 4k or 2k version available

### [JS Helm of Yngol](https://www.nexusmods.com/skyrimspecialedition/mods/51346)

Texture replacer for the helm of yngol

**NOTES**

* 2k and 4k version available

## Animals and Creatures Texture replacer

### [Real Rabbits SE](https://www.nexusmods.com/skyrimspecialedition/mods/29223?tab=description)

**NOTES**:

* Required for Dynamic Animal Variants SE
* Real Rabbits Diverse must be downloaded
* Mods that use the vanilla HareRace will automatically use these textures
* Mods that completely change the mesh and use a different UV mapping than vanilla will not be compatible.
* Make sure it loads after SMIM
* SMIM has a patch for Real Rabbits SE


### [Dynamic Animal Variants SE](https://www.nexusmods.com/skyrimspecialedition/mods/34763?tab=description)

**NOTES**:

* Uses BellyAches Textures
* Requires Real Rabbits
* Any retextures have a chance to appear
* Any mod that change NPC and animal records changed by this mod will have conflicts

**Patches**: 

* Daedric Beasts
* Mortal Enemies SE
* No Spinning Death animation
* Realistic Wildlife Behaviours
* Skyrim Revamped - Complete Enemy Overhaul
* Realistic Animals and Predators SE
* Violens
	 * Wild World SE

### [Immersive Dragons](https://www.nexusmods.com/skyrimspecialedition/mods/18957?tab=description)

**NOTES**: 

* Mods that make changes to dragon skeletons should be overwritten by Immersive Dragons
* Compatible with any texture mods


### [BellyAches HD Dragon Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/2636)(NOT USING)

**NOTES**:

* Un-potatoes the dragons


### [HD Reworked Dragon Collection 4K](https://www.nexusmods.com/skyrimspecialedition/mods/36038?tab=files&file_id=140119)

**NOTES**: 

* Vanilla friendly 4k dragon replcer
* Covers dragonbones, scales, heartscales



### [Bears of the North](https://www.nexusmods.com/skyrimspecialedition/mods/47541)

**NOTES**: 

* Compatible with anything that doesn't touch vanilla bear records

### [Fluffworks](https://www.nexusmods.com/skyrimspecialedition/mods/56361?tab=files)

**NOTES**: 

* This mod in the current stage is incompatible with other retextures, however retextures which are vanilla upscales or follow the vanilla style (like HalkHogan's HD Reworked mods) are compatible
* Choose the quality version
* Overwrite other retextures with this mod
* Overwrite this mod with the bear mod\

**PATCHES**

* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/64445?tab=files&file_id=275187&nmm=1) for;
- AN Horse
- Animalica full or dear only or Lore friendly redux version or lore friendly version
- Better Cows
- CC Pets of Skyrim
- CC Wild Horse
- CC Wild Horse Patch(No unicorn)
- MM Real Elk''s
- MM White Stag Male Elk or Reindeer
- Realistic Hores Breeds
- Shuffler90's Elk and Deer
- Skeever to Rat blakc and White
- Skeever to Rat Brown
- Skeever to rad gray 
* [Immersive Horses - SC Horse ](https://www.nexusmods.com/skyrimspecialedition/mods/63358)
* [Immersive Horses](https://www.nexusmods.com/skyrimspecialedition/mods/65617)

### [Realistic Horse Breeds](https://www.nexusmods.com/skyrimspecialedition/mods/7685)

**NOTES**

* Should be installed after all other horse mosd unless specifically told not to
* Any horse mods which change the horse MESH are not directly compatible as this mod introduces new .nif copies for EACH breed of horse.
Most horse mods only use ONE .nif mes
* Blue eye optoin


## [Rougeshots Skeleton replacers for creatures]

**NOTES**:

* These mods are compatible with anything that don't interfere with the kanilla sekeltons
* Install XP32MSE versions of these mods when available
* Supreme Vampire Lords
* Savage bear - replaces mesh
* Grave Gargoyles
* Nightmare Chaures
* Tyrannical Trolls
* Gardiose Giants
* Absolute Arachnophobia
* Supreme Chaurus hunters
* Wicked Werewolves
* Looming Lurkers
* Marvelous Mudcrabs
* Mighty Mammoths
* [ ] Supreme Seekers
* Bristleback Boars
* Heinous Ash Hoppers
* Honored Hounds
* Hardy Hares
* Sickening Skeevers
* Riekling Roughriders
* Sinister Spriggans
* Gritty Goats
* Astonishing Frost Atronachs
* Savage Wolves
* Dreaded Dwarven Spiders
* Notorious Netches
* Salty Slaughterfish
* Infamous Ice Wraiths
* Hulking Horkers
* Dramatic Deer
* Feral Foxes
* Bullish Bovine
* Hellish Hounds
* Callous Dwemer Centurions
* Astounding Flame Atronachs
* Supreme Dwemer Spheres
* Heartland Horses
* Marvelous Mudcrabs
* Mighty Mammoths

### [Uniqe Barbas retexture](https://www.nexusmods.com/skyrimspecialedition/mods/17540)

**NOTES**: 

* This look is based on how Barbas appeared on the ESO Summerset Cinematic

### [HD Reworked Mammoths](https://www.nexusmods.com/skyrimspecialedition/mods/55479?tab=files)

**NOTES**

* Nothing to add really


### [Frankly HD Dragon Bones](https://www.nexusmods.com/skyrimspecialedition/mods/25099)

**NOTES**: 

* Choose 4k-2k version


### [Skeleton Replacer HD - SE](https://www.nexusmods.com/skyrimspecialedition/mods/52845)

**NOTES**:

* Download 2k SSE
* The skulls for unique characters are covered in Unique Skulls HD - SE
       
**PATCHES**:

* Beast Skeleton revised(bitter edition)
* M'rissi's Tails of Troubles
* WACCF
* Skeleton eyes Enb-Light patch (you still need Particle Lights For ENB SE - Undead Creatures if you want the female draugrs,dragon priests and ghosts to have enb-light as well           , just let my enb-light patch overwrite it).﻿
* Undeath
* Majestic Mountains
* Particle Lights for ENB
* Rustic Reliefs
* Draugr Upgrades and Improvements (Draugr and Skeleton Overhaul).
* Clockwork
* Undead FX
* ELFX
* Path of Sorcery
* Extra playable skeletons SSE
* Skyrim immersive creatures
* The tools of - ux
* Deaths head Skull mask
* Sacrosant Vampires of Skyrim
* Sacrilege Minimalistic vampires of Skyrim
 
### [Beast Skeletons Revised (Bitter Edition)](https://www.nexusmods.com/skyrimspecialedition/mods/27266?tab=description)

**NOTES**: 

* Not compatible with Immersive Creatures

**Patches**:

* Draugr updates and improvements


### [Fluffworks - Better Photoreal Foxes](https://www.nexusmods.com/skyrimspecialedition/mods/65974)

**NOTES**:

* Nothing to add
* Sweet roll eyes fix

## Mod Retextures

### [SkyTest - Mihail's Gray Farm Goose SE](https://www.nexusmods.com/skyrimspecialedition/mods/35565)
* Options for Lower idle sound or Mihail idle sound

### [Skytest - Mihail's Chick Replacer and Better Idle Sounds](https://www.nexusmods.com/skyrimspecialedition/mods/35634)

### [Skytest - Less Shiny Pigs](https://www.nexusmods.com/skyrimspecialedition/mods/35580)

### [Skytest - HD Horkers SE](https://www.nexusmods.com/skyrimspecialedition/mods/35662)

### [SkyTEST - Bellyaches HD collection SE](https://www.nexusmods.com/skyrimspecialedition/mods/35719)


# Environment

## Lighting

### [Lux](https://www.nexusmods.com/skyrimspecialedition/mods/43158)

**NOTES**

* Download Lux resources from the lux via page
* Not compatible with any lighting mod or mods that add lights.
* Lux already includes ENB changes such as sky and weathers cells parameters.
* Sounds and music mods should need a compatibility patch.
* Skyrim Project Optimization will conflict
* Mods that do patch ELFX meshes should overwrite Lux

**FOMOD**

* Creation Club Meshes
* Optional Effects
- Particle without effect lighting - yes
- Mists and fogs - particle without effect light- Webs - yes
* SMIM Meshes - yes
* Peltapalooza Pelts - yes
* Rugnarok Rugs -no
* SD's Table replacer - vanilla
* Lux Optimized chandeliers - SMIM + RCI
* Lux optimized Embers meshes
* Lux optional giant campfires
* Brighter Temples - none, this is only for non-enb users
* Even Brigter lighting temples - same as above
* Language - English

**PATCHES**

* JK's College Patch update
* LOTD updated safehouse
* Rodryk's and JK's Skyrim Dragon Bridge updated patch
* Player House Patches in FOMOD  
                   1. No Grass In Caves
				   2. Eli Skaal You Need
				   3. Eli Riverwood Shack 
				   4. Eli Routa
				   5. Eli Ruska
				   6. Eli Sicarius Refuge
				   7. Eli Halla
				   8. Eli Tirashan
				   9. Eli Mage Tower
				   9. Eli Breezehome
				   10. Wynter Breezehome
				   11. TNF Houses Remodel
				   12. Honeyside TNF
				   13. Ravens Breezehome
				   14. Riverside LOdge
				   15. Antennaria
				   16. Whiterun Well Home
				   17. Hunters Cabin of RIverwood
				   18. JK's Riverfall dottage
				   19. Hidden Hideouts of Skyrim
				   20. Unique Vampire Dens
				   21. Bal Ruhn
				   22. Dol Khazun
				   23. MyrHemir
				   24. Zulfardin
				   25. Alchemy Lab
				   26. Wind Path
				   27. Winter Cove
				   28. Solitude Rectory ESPPE
				   29. Elysium Estate
				   30. Elysium Estate 4.3.6
				   31. Sutkaka
				   32. Rayeks End SSE
				   33. Windyridge 
				   34. Redoran Grove
				   35. Ebongrove
				   36. Plague and POison
				   37. Shurrashi's Den
				   38. Kel Fishing Shack
				   29. Eli's Tinies Shack
				   30. Breezehome TNB Bed plus 
				   41. Winterbery Chateau
				   42. White Lighthouse 
				   43. Sindora's Hidden Harth
				   44. Leaf Rest 
				   45. Stalker's Refuge Vampire House 
				   46. Simple Player Homes Imbprovements
				   47. Solitude Rectory
				   48. Solitude Rectory ESPFE
				   49. Solitude Recotory JK's Temple
				   50. Frostwood Cabin
				   51. Astronomers loft
				   52. Silverspeak Lodge 
				   53. Riften Sewer Hideout
				   54. Autumngate 
				   55. Trollpacka
				   56. Milandriel
* Other pathes in FOMOD 
                        1. Cutting Room Floor
						2. Places and Castles Enhanced(optoin for PCE no costum music) 
						3. Great Villages and Towns(whole series)
						4. RedBag's City overhauls Rorikstead and Solitude
						5. Enhanced Solitude
						6. AssyMcGees Solitude Docks 
						7. Blue Palace Terrace
						8. Shilzohr 
						9. Alternate Start 
						10. Arhtmoor Oakwood
						11. Arhtmoor Keld-Nar
						12. Arhtmoor Shors Stone
						13. ArhtmoorDawnstar
						14. Arhtmoor Ivarstead
						15. Arhtmoor Dragon Brideg
						16. Arthmoor Falkreath
						17. Arthmoor Granite Hill
						18. Soljund's Sinkhole
						19. Arhtmoor Whistling mine
						20. Arhtmoor Kynesgrove
						21. Arhtmoor Karthwasten
						22. Darkwater crossing
						23. Telengard 
						24. HElarchen
						25. Provincial Couriers service
						26. Castle Volkihar Rubuilt
						27. JK's Skyrim
						28. Ideal Riverwood
						29. JK"s interiors(all)
						30. Settlements Expanded
						31. Stendarr Rising
						32. Cities of the Norsth
						33. Expanded Towns and Cities
						34. ClefJ Overhauls
						35. Capital Cities Expansion
						36. Windhelm Gray Quarters
						37. Whiterun Metropolis
						38. Dark's Whiterun Market
						39. Quant Raven Rock
						40. Better Tel Mithryn
						41. Eli Skaal Vilalge overhal
						42. Greater Skaal Village
						43. Obscure College of Winterhold
						44. Ultimate College of Winterhold
						45. Rodryk's Dragonbridge(Arhtmoor variant avalable)
						46. Distict Intriors
						47. Bards reborn
						48. Outlaw Refuges
						49. Dugeons Rivisited
						50. Easirrider's Dungeons
						51. The Midden
						52. Tools of Kagnerac
						53. Interesting NPC's
						54. Azura Srhine
						55. Legacy of The DragonBorn
						56. Markarth Bookstore
						57. Moon and Star
						58. Mrissi Tail of Troubles
						59. People of Skyrim Complete
						60. RIverwood Redone
						61. Jorvaskr Redone
						62. This is Jorvasskr
						63. Wayshrines
						64. Fortune Trade House
						65. Windhelm Entrance Overhaul
					66. Books of Skyrim
						67. Forgotten CIty
						68. Wyrmstooth
						69. Clockwork
						70. Project AHO
						71. Skyrim Sewers
						72. MS master collection
						73. Landscape and water fixes
						74. DesertX's Crimson blood Armor
						75. Missing Encounter Zones fixed
						76. USSEP
						77. EmbersXD
						78. RW2
						79. Sounds of Skyrim
						80. Audo Overhaul Obsidian Weathers
						81. Audio overhaul SOS
						82. Immersive Citizens
						83. Wintersun
						84. Pilgrim
						85. Alternate Perspective
						86. Helgen Rebonr
						87. Helgen Reborn COTN Falkreath
						88. The Stumbling Sabrecat
						89. 3DNPC's Alternative Locations
						90. RYFTEN Consistency of Windows
						91. RYFTEN LK's BEE and the Barb
						92. RYFTEN JK's Haelga Bunkhouse
						93. High Hrothgar Kitchen 
						94. Pentus Oculatus
						95. Acadia's Caludron
						96. Belethors General Goods
						97. Bannered Mare
						98. Warmaidens
						99. Distinct JK's Arnleif and Sons Trading Company
						100. Great city of JK's Redbag Solitude
						101. COTN - ETaC Danwstar Modular
						102. COTN + ETaC Dawnstar complete
						103. JK's skyrim and Rodry'k's DB
						104. Morthals where wares
* CC Content
				1. Ghostsof the Tribunal
				2. Umbra
				3. Saints and Seducers
				4. Bittercup
				5. Dead Mans Dread
				6. Be a Farmer
				7. Shadowfoot Sanctum
				8. Nchuanthumz Dwarven Home
				9. Necrohouse
				10. Tunra Homestaed
				11. The Cause
				12. Hendraheim 
				13. Bloodchil Manor
				14. Elite Crossbows
				15. Forgotten Seasons
				16. Myrwatch Tower
				17. Pets of Skyrim
				18. Staff of Hasedoki
				19. Vigil Enforcer
				20. Dawnfang
* Minor Locations 
                  1. Dibellan Baths
				  2. Divilen Elgance
				  3. Flying Knight Tavern
				  4. Flying Knight Davern Keep it claen
				  5. Beyond Skyrim Wares of Tamriel
				  6. Thogra
				  7. The Breson Paldin
				  8. Tournament of ten bloods
				  9. Ice blade of the monarcks
				  10. Inigo
				  11. Old Hroldan
				  12. Kep it clean OCW Bath
				  13. Song of tHE Green UARi
				  14. Shrine of Sanguine
				  15. Pinehelm
				  16. Peastess Laire
				  17. Expanded Dragon Bridge
				  18. Watchtowes of Skyrim
				  19. Lucien

				





### [Relighting Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/8586)(NOt using)

**NOTES**: * Should be loaded after weather mods
       * Should be loadde before lighting mods
       * Compatible with 1. Enhanced Lighting for ENB Cathedral Weathers and Seasons
                         2. CLARALUX SSE - Controllable Lights Everywhere
			 3. Climates of Tamriel
			 4. Darker Nights
			 5. Dolomite Weathers - NLVA II
			 6. Dynamic Immersive Seriously Dark Dungeons - SSE
			 7. IMAGINATOR - Visual Control Device for Skyrim
			 8. Interior Floating Fog Remover
			 9. Luminosity Lighting Overhaul - The Cathedral Concept
			 10. Photorealistic Tamriel
			 11. Reduced Intensity ImageSpace Settings
			 12. Rustic Weathers And Lighting
			 13. Surreal Lighting -- vibrant and cheerful weathers and lighting
			 14. True Storms Special Edition - Thunder Rain and Weather Redone
			 15. Vivid Weathers Special Edition - a complete Weather and Visual overhaul for Skyrim SE
       * Not compatible with 1. RLO 
                             2. ELFX 
			     3. Improved Hearthfire Lighting 
			     4. Dynamic Shadows SKyrim
       * [Tutorial](https://www.youtube.com/watch?v=4ZF_iKd_5XI) for patching this with a given load order


### [Enhanced Lighting for ENB (ELE) - Special Edition](https://www.nexusmods.com/skyrimspecialedition/mods/1377)

**NOTES**:

* Patches and this mod should be loaded after Weather mods
* Same as above for light bulb mods
* Smarhed or Bashed patches should be loaded after this mod
* Not Compatible with;
                      1. RLO
                      2. ELFX
		      3. Reduced Intensity ImageSpace Settings
* Compatible with;
                  1. Relighting Skyrim - SSE
                  2. Improved Heartfire lighting Skyrim
		  3. Dynamic Shadows for Skyrim
		  4. Climates of Tamriel
		  5. Enterior Floating Fog remover
		  6. CLARALUX SSE - Controllable light everywhere
		  7. Rustic weathers and lighting
		  8. IMAGINATOR
		  9. Dolomite weathers
		  10. Surreal Lighting
		  11. Vivid Weathers
		  12. Tru Storms SE - Thunder and weather visual overhaul for Skyrim SE
		  13. Photorealistic Tamriel
		  14. Darker Nights
		  15. Dynamic Immersive Seriously Dark Dungeons - SSE
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/27667?tab=description) for;
                   1. JK's Skyrim
		   2. Keld-Nar
		   3. Solitude Docks
		   4. Provincial Courier Service 
		   5. Ivarstead
		   6. Shors's Stone
		   7. Kynesgrove
		   8. Falkreath
		   9. Soljund's Sinkhome
		  10. Dawnstar
		  11. Solitude Exterior addon
		  12. Rorikstead
		  13. Bring Out Your Dead
		  14. Cutting Room Floor

* And [here](https://www.nexusmods.com/skyrimspecialedition/mods/18369) for;
               1. Immersive College of Winterhold
               2. Realistic Water Two

### [ENB Helper](https://www.nexusmods.com/skyrimspecialedition/mods/23174)

**NOTES**: 

* Download the AE version

### [Particle Patch for ENB](https://www.nexusmods.com/skyrimspecialedition/mods/65720)

**NOTES**: 

* Let any other mod overwrite this mod

## Weather

### [Cathedral Weathers](https://www.nexusmods.com/skyrimspecialedition/mods/24791)(NOT USING)

**NOTES**:

* Not compatible with other weather mods
* to patch a mod like true storms, you may wish to create a standalone esp that depends on the sound files found in true storms
* Compatible with audio overhauls with patch.
* Compatible with Obsidian Mountain Fogs. Excluded by special request.
* Safe to overwrite other weather mods

### Obsidian Weathers

**NOTES**:

* Compatible with everything except weather mods
* Not compatible with mods that make nights darker unless they're made specifically for Obsidian Weathers (these are fundamentally weather mods)
* Not compatible with storm mods without a patch (these are weather mods)
* Uses only vanilla weather ID's ensuring full compatibility with mods like Frostfall, Wet and Cold, Wonders of Weather, and Beyond Skyrim
* Seasonal weathers require modification to region records and thus requires a patch for audio overhauls that add new region sounds
* Compatible with all interior lighting overhauls (ie, ELE, RS, ELFX, RLO). Minor adjustments made to torches and magelight to balance darker nights. 

**PATCHES**: 

* True Storms

### [Natural and Atmospheric Tamriel](https://www.nexusmods.com/skyrimspecialedition/mods/27141)(Not Using)

**NOTES**

* Incompatible with  Any weather mod (exterior fog or night modifications are weather mods)
* Incompatible with particle patch for ENB(Since int's includded)
* Incompaitleb with thunder and rain retextures and sounds
* Any sky or cloud meshes
* Incompaitleb with dynamic volumetric lights and shadows
* ENB lights is recommended
* NAT-ENB.esp PLUGIN IS NOT MEANT TO BE USED WITHOUT THE NAT.ENB preset,
* NEITHER THE ENB PRESET CANNOT BE USED WITHOUT THE NAT-ENB.esp PLUGIN.

### [Wonders of Weathers](https://www.nexusmods.com/skyrimspecialedition/mods/13044)

**NOTES**:

* Compatible with all mods. Load order doesn't matter.

**FOMOD**:

* No optionals should be chosen


### [Enhanced Volumetric Lights and Shadows](https://www.nexusmods.com/skyrimspecialedition/mods/63725)

* Requires SKSE 64 and Adresss libarry
* It is compatible with anything


### [ENB Lights](https://www.nexusmods.com/skyrimspecialedition/mods/22574)

**NOTES**

* Complex Particle Lights must be enabled
* Complex Fire Lights must be DISABLED if you use my fire lights
*  Complex Particle Lights - DistanceFadexxx settings (all of them) must be set to minimum (0.3) and Big Range enabled to achieve the maximum possible light radius. Intensity settings may need to be adjusted if the light is too bright or too dim. My intensity settings are 0.4 (the new Rudy ENB defaults).


**FOMOD**

* Between Minimal and Full choose full install
* Addition options 
					- Minor effects - yes
					- Extra light - no 
					- Magic hand effects - yes
					- Magic weapon effects - yes
					- Fire magic projectiles  - no
					- Magic hazards and burniing fires -no
					- Magic effects - no
					- Dragon fire - no
					- Creatures - Flame atronacs - yes
					- creatures - spriggans yes
					- Creatures wispmother - yes
* Chose Dawnbreaker ENB light 

**PATCHES**

* IN fomod
			- Dawnbreaker Better shaped weapons shaets
			- Dawnbreaker better shaped weapons dsr meshes
			- Smoking Torches and Candles
			- Torches Cast Shadowes
			- Both of the above
			- Embers HD(2k, Orange - 2k, HD, Fireplaces, Fireplaces + custom fire effects, custom fire effects with STAC and TCS, or all)
		

# World

## City Overhauls

### [Capital Whiterun Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/37982)

**NOTES**:

* Download the normal version
* Has exterior compatibility version too
* Expans whitrun inside and outside
* A new quest has been added
* Mods editing vanilla NPC should be OK.
* Mods editing texures should be OK
* Mods editing only interiors should be OK
* Mod's editing objects or navmesh in Town will likely need a patch
* Immersive Laundry - Compatible
* AI Overhaul - Compatible
* Bells of Skyrim - Compatible
* Astronomer's Loft - Compatible
* Download the boardwalk disabler [here](https://www.nexusmods.com/skyrimspecialedition/mods/52622?tab=files&file_id=217573)

**PATCHES**:

* Wind Districts Breezehome
* Whiterun - The city of walls
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/38215?tab=files) for:
                   1. Dawn of Skyrim
		   2. JK's Bannered Mare
		   3. JK's Drunken Hutsman
		   4. JK's Skyrim
		   5. JK-DOS
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/42766) for:
                   1. Cutting room floor
		   2. Whiterun Riverside Expansion
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/52622?tab=files) for: 
                   1. Fortified Whiterun
		   2. Brothermood of old
		   3. Grass patch
		   4. LoS II 
		   5. Verdant
* RS Children overhaul
* Provincial Courier service(Only for the normal version)


### [Capital Windhelm Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/42990)

* Download the normal version
* Adds 40 new NPC's
* Adds stuff outside the walls
* Adds 8 new quests
* Mods editing vanilla NPCs should be OK
* Mods editing texures should be OK
* Mods editing only interiors should be ok
* Mods like JKs, DoS and others that move things around outside or place new objects will likely need a patch.
* Mods that edit Windhelm navmesh will likely need a patch.
* If you have mods that adds a lot of NPCs you might need [this](https://www.nexusmods.com/skyrimspecialedition/mods/32349)
* Incompatible with Bells of Skyrim
* Incompatible with Open Cities
* Incompatible with Snowy AF

**PATCHES**: 

* Cutting room floor
* AI Overhaul
* Embers XD
* JK's Skyrim
* High Poly NPC and KS hair JS
* Clockwork
* ICAIO
* OutlawsRefuges
* SkyrimSewers
* Windhelm gate fixes
* Danw of Skyrim
* Windhelm bridge tweaks
* [Khajit Speak](https://www.nexusmods.com/skyrimspecialedition/mods/44742?tab=files&file_id=181377)
* [Narrative Loot](https://www.nexusmods.com/skyrimspecialedition/mods/45315)
* [RS Children](https://www.nexusmods.com/skyrimspecialedition/mods/45321/?tab=posts)
* [Guard Armor Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/45439?tab=files)
* [City entrances Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/47186?)


### [Windhelm Expansion - Grey Quarter](https://www.nexusmods.com/skyrimspecialedition/mods/42234)

**NOTES**

* 20+ new Interiors
* 50+ new NPC
* A new Inn
* New Shops
* A small player home
* Adds completely "new District" to Windhelm which expands the Grey Quarter
* Most changes happen in a new worldspace for better performance and easier Compatibility
* Every mod that changes Windhelm should be loaded after this mod

**PATCHES**

* Capital Windhelm Expansion
* JK's Skyrim
* Dawn of Skyrim Directors cut
* Windhelm exterior altered
* Windhelm Docks pathway
* Better Docks
* Nernies Thief pack
* Gray Quarter Overhaul


### [Enchanced Solitude SSE](https://www.nexusmods.com/skyrimspecialedition/mods/27816)(Not Using)

* Compilation of multiple Solitude mods
* [These](https://www.nexusmods.com/skyrimspecialedition/articles/1294) mods are alreadly included in this mod
* Mods which edit Solitude City are likely INCOMPATIBLE with Enhanced Solitude
* Mods which edit Solitude Docks should be COMPATIBLE with Enhanced Solitude
* Mods which edit Solitude Docks should be COMPATIBLE with the Enhanced Ship module
* Better Docks is COMPATIBLE
* ELFX is COMPATIBLE
* Blowing in the Wind is COMPATIBLE
* Immersive Citizens AI Overhaul is COMPATIBLE by following Load Order Rules
* Solitude Skyway is COMPATIBLE 
* Proudspire Manor mods are COMPATIBLE 
* Palaces and Castles Enhanced is COMPATIBLE
* Kainalten Keep is COMPATIBLE
* The Great City of Solitude is COMPATIBLE
* Realistic Solitude Arch is COMPATIBLE
* TPOS2 is COMPATIBLE
* Another Skyrim Solitude is NOT COMPATIBLE
* Towns and Villages Enhanced - Solitude is NOT-COMPATIBLE
* Download option for Cherry Trees
* Dowload option for ES Exterior Boost Patch
* Dowload option for ES Terrain Patch


**PATCHES**

* Serinity
* Solitude Exterior Add on
* LoTD
* Solitude Expansion
* Dev Azeza
* Blue Pallace Terrace
* Palaces and Castles enhanced
* Alternate Main wall textures
* The Flora of Skyrim Hand placed Uniques
* Bells of Skyrim
* Outlaws Refugees
* Imperial Mail
* USSEP
* Open Cities
* ES Redbag(only lowers the walls)
* Enhanced Solitude Expansion
* Enhanced Trees of Solitude
* DOS
* Immersive Laundry
* JS DoS
* JK Skyrim AIO
* LoS II TML
* RS Childrens
* Shadows of the past
* Storefront
* TFoS Cherry TRees


### [Enhanced Solitude Docks](https://www.nexusmods.com/skyrimspecialedition/mods/28349)

**NOTES**

* Compatible with Enhanced Solitude
* Combaitle with  Kainalten Keep
* Compatible with Lanterns of Skyrim 2
* Incompaitible with Haafinheim
* Incompatible with Serenity
* Incompatible with Priority of the cape
* Incompatible with Solitude Docks District
* Incompatible with The Great City of Solitude

**PATCHES**

* Enhanced Solitude
* LoS II
* Medieval Lanterns of Skyrim
* eFPS
* Open Cities
* SMIM



### [The Great Cities - Minor Cities and Towns SSE Edition](https://www.nexusmods.com/skyrimspecialedition/mods/20272?tab=description)

**NOTES**

* This mod is incompatible with any mod that significantly alters the exterior area surrounding Dawnstar, Dragon Bridge, Falkreath, Morthal, Rorikstead or Winterhold.
* This mod does NOT overaul the five major cities Markarth, Riften, Solitude, Whiterun or Windhelm.


## Interiors

### [JK's Dragonsreach](https://www.nexusmods.com/skyrimspecialedition/mods/34000)

**NOTES**

* Compatible with enhanced lights and FX
* Compatible with Palaces and Castles enhanced sse
* Not compatible with mods that overhaul the interior of dragonreach with the exception of texture mods


### [JK's Blue Palace](https://www.nexusmods.com/skyrimspecialedition/mods/45324?tab=description)
**NOTES**

* Compatible with Window Shadows by Dlizzio. Compatible with patch on mod page.
* Compatible with Lux, patch on mod page
* Might be reference fix so install the refernce fix file

### [JK's Palace of the Kings](https://www.nexusmods.com/skyrimspecialedition/mods/48902)
### [JK's Jorrvaskr](https://www.nexusmods.com/skyrimspecialedition/mods/60738)
### [JK's High Hrothgar](https://www.nexusmods.com/skyrimspecialedition/mods/62219?tab=files&file_id=265792&nmm=1)
### [JK's Interior Patch Collection](https://www.nexusmods.com/skyrimspecialedition/mods/35910?tab=description)
**NOTES**

* Remove duplicate unique items - yes
* Rugnarok - yes / **no**
* JKs Blue Palace
- AI Overhaul
- Amulets of Skyrim
- Animated Armoury
- BadGremlin's Collectibles
- Cheesemod for EVERYONE
- Elder Scrolls Onlone Imports
- Extravagant Blue Palace
- Lanterns of Skyrim II
- LOTD Collectors and Vendors
- Mystic Condenser
- Red Flame
- Royal Armory 
- Skyrim Artwork Imports
- Skyrim Unique Treasures
- Tamrielic Culture
- USSEP
- 3DNPC and SFCO - 3DNPC / Snazzy Furniture and Clutter Overhaul / 3DNPC - Snazzy Furniture and Clutter Overhaul / **None**
- DX Dark Knight - DX Dark Knight Shield Standalone / DX Dark Knight + Armor Merge / **None**
- Enhanced Lights and FX - Enhanced Lights and FX / ELFX Enhancer / ELFX Hardcore / **None**
- Solitude City Mods- Enhanced Solitude - Legacy of the Dragonborn - Solitude Skyway \ Enhanced Solitude - Solitude Skyway\ JK's Skyrim - Legacy of the Dragonborn - Solitude Skyway\ JK's Skyrim - Solitude Skyway/ Legacy of the Dragonborn - Solitude Skyway/ Enhanced Solitude/ JKs Skyrim/ Solitude Skyway/ **None** / Optional files(Created copy of Gilded Wristguards named Fancy Wristgards, replaced gilded wristgards with it. Firebrand Wine -> Spiced Wine) - yes
* JKs Dragonsreach
- AI OverHaul
- Art of Magicka
- BadGremlin's Collectables
- Cheesemod for EVERYONE
- Immersive Citizens - AI OVerhaul
- Immersive Wenches
- Konahrik Accoutrements
- Levathan Axe - Dwemer Artifiacs SE
- Mystic Condenser
- Populated Cities and Towns
- Relatoinship dialogue overhaul
- ScrolllScribe
- Second Great War
- Skyrim Artwork Imports
- Skyrim Unique Treasuers
- Snazzy Furniture and Clutter Overhaul
- Tamrielic Culture
- Enhanced Lights and FX - Enhanced Lights and FX / ELFX Enhancer/ ELFX Hardcore / **None**
* JK's Palace of Kings 
- BadGremlin's Collectibles 
- Cheesemod for everyone 
- Cloaks of Skyrim
- DX Faction Crossbows 
- EasierRider's Dungeon Pack
- Elder Scrolls Online IMports
- Embers XD
- Holidays
- Ommersive Citizens - AI Overhaul
- Lanterns of Skyrim II Windhem Brazier Replacer
- Populated Towns and cities
- Second Great War
- Skyrim Artwork Imports
- Skyrim Project Optimization
- Skyrim Sewers
- Skyrims Unique Treasuers
- Snazzy Furniture and Clutetr Overhaul
- Ursine Armor
- USSEP
- Enhanced Lights and FX - None
- Guard Armor Replacer - Full / Minimal / **None**

### [Perseids Inns and Taverns - Realistic Room Rental Enhanced](https://www.nexusmods.com/skyrimspecialedition/mods/24859)
**NOTES**

* The Enhanced version is NOT compatible with any other mods that also modify the interiors of vanilla inns
* Distinct Interiors IS compatible as long as you install the modular versions and DO NOT install the Inn and taverns modules
* This mod is compatible with the no-inn version of ETaC (in SLE only)
* There are some mesh incompatibilities between high poly project and this mod, overwrite this mod with high poly project
* Incompatible with Jobs of Skyrim
* NON-CRF versoin available
* Download the update


**FOMOD**

* Core files - Basic/ **Ehanced**/ Enchanced(NSUtR compatible)

**PATCHES**

* ELFX
* Keep it Clean
* ICAIO
* Immersive Citizens
* Interestic NPC's
* Non-CRF version
* Alternate Start patch
* In FOMOD
- Interesting NPC's
- Immersive Citizens AI Overhaul
- Alternate Start
- Keep it Clean
- Enhanced Light and FX
- More Tavern Idles(Don't select this if using hte original mod)

 

## Creatures

### [House Cats- Mihail Monsters and Animals](https://www.nexusmods.com/skyrimspecialedition/mods/30496)
Adds Lore-Friendly House Cats to Skyrim

### [Immersive Horses](https://www.nexusmods.com/skyrimspecialedition/mods/13402)

**NOTES**
 
* Options for 2k horse textuers or vanilla horses

**PATCHES**

* [Beyong Skyrim Bruma](https://www.nexusmods.com/skyrimspecialedition/mods/21121)
* [Wild Horses CC](https://www.nexusmods.com/skyrimspecialedition/mods/56696)
* [Light Horses](https://www.nexusmods.com/skyrimspecialedition/mods/41351)
* [Saturalia Holiday CC](https://www.nexusmods.com/skyrimspecialedition/mods/27357)
* [Fluffworks - SC Horses](https://www.nexusmods.com/skyrimspecialedition/mods/63358)
* [Forgotten Season SC](https://www.nexusmods.com/skyrimspecialedition/mods/52331)
* [Fluffworks](https://www.nexusmods.com/skyrimspecialedition/mods/65617)
* [Blakcthorn](https://www.nexusmods.com/skyrimspecialedition/mods/54408)
* [Capital Whiterun](https://www.nexusmods.com/skyrimspecialedition/mods/66709)

### [Immersive Horsse Script Fixes](https://www.nexusmods.com/skyrimspecialedition/mods/40543)

Fixes some bugs

**PATCHES**

* SkyHUD

### [Skytest - Realistic Animals and Predators](https://www.nexusmods.com/skyrimspecialedition/mods/1104?tab=description)
**NOTES**

* This mod will conflict with any mod that edits Animal races, actors and factions
* AE and SE versions available
* Optional Files for
- Anations Fix for Mute Wolves
- SEQ File to fix onconsistensies with the mod
- Lite Versoins
- inAWussy Edition

**PATCHES**

* [Project AHO](https://www.nexusmods.com/skyrimspecialedition/mods/24997)
* [Less shiny pigs](https://www.nexusmods.com/skyrimspecialedition/mods/35580)
* [Mortal Enimiies Patch](https://www.nexusmods.com/skyrimspecialedition/mods/25624)
* [Bears of the North](https://www.nexusmods.com/skyrimspecialedition/mods/47708)
* [Riften Puppies](https://www.nexusmods.com/skyrimspecialedition/mods/30334)
* [Diversified Chicken and Skytest chicks](https://www.nexusmods.com/skyrimspecialedition/mods/57168)
* [No Tree Patch Fix for DynDOLOD](https://www.nexusmods.com/skyrimspecialedition/mods/57358)
* [Creatures Multi-patch - SkyTEST-Savage Skyrim-Attack Dogs-Diseased Remastered and Extended and more](https://www.nexusmods.com/skyrimspecialedition/mods/65670)


### [SkyTEST Integration Project SE](https://www.nexusmods.com/skyrimspecialedition/mods/13428)

**NOTES**

* DLC and Vainlla creature extionsion available for download


**PATCHES ADDED BY THIS MOD**

* Animallica
* Beasts of Tamriel
* Immersive Horses
* Real Wildlife Skyrim
* No Extra Spawns



# NPC Overhaul   

## Body Textures and Meshes

### [XP32 Maximum Skeleton Special Extended](https://www.nexusmods.com/skyrimspecialedition/mods/1988)

**NOTES**

* Requires Fores New Idles(But actually use NEMESIS like a boss)
* Make sure XPMSE overwrites FNIS(NEMESIS)
* Extended Skeleton for SSE
* Overwrite Schlongs of Skyrim with this
* Let other Skeleton mods overwrite this mod unless specifically asked


**FOMOD**

* Animation Rig Map Physics Extensions/Old/Fake/no physics/none - Physics extensions
* Character Creation, Racemenu/ECE/MCM/None - Racemenu
* Weapon Style Randomizer, In Sync/In of Sync(SFW)/Out of sync/Ouf of sync SFW/None - None
* Belt fastened quivers - none
* daggers on back - none
* dagger on hip - none
* Magic - none
* Sword on back - none
* Sword on hip - none
* first person axes on back - no
* first person Sword - no
* MOunted axes on back - no
* Mounted belt-fastened quivers - no
* Mounted Sword - no

**PATCHES**

* in FOMOD;
	    1. Deadly Mutilition
            2. Joy of Perspective
	    3. Schlongs of Skyrim
	    4. Enderal


### [CBBE](https://www.nexusmods.com/skyrimspecialedition/mods/198)

**NOTES**

* If intend to use physics bodd, XPMSSE is absolutely required

**FOMOD**

* Body type - Slim/Curvy/**Vanilla Shape**
* Underwear options - Underwear/NeverNude/**None**
* Vanilla outfits - yes
* Face options - No
* Eyebrows options - No
* Dirt to Beauty Marks - no
* Pubic hair - Yes
* SKEE - No
* MORPH files - yes

### [Faster HDT-SMP](https://www.nexusmods.com/skyrimspecialedition/mods/57339)

**NOTES**

* Download the AE version and the XML files for CBBE and CBBB 3A
* This mod doesn't require HDT-SMP

**FOMOD**

* SE AE VR - **SE** / AE / VR
* CUDA - NO
* AVX - AVX
* Dbug - Release

### [CBPC](https://www.nexusmods.com/skyrimspecialedition/mods/21224)

**NOTES**

* Requires SKSE and XP32
* Requires NEMESIS
* Make sure absolutely nothing overwrites this mod unless told to
* Don't use CBP
* Not compatible with realistic ragdolls and force
* Not compatible with dragonfly bbp
* Not compatilbe with player size adjuster and firts person camera eight six sse


**FOMOD**

* Game - SE
* SSE versoin - whatevers latest
* Body Shape - CBBE slim
* FPS - 60
* Bounce config - 5
* Gravity config - yes

### [CBBE 3BA](https://www.nexusmods.com/skyrimspecialedition/mods/30174)

**NOTES**

**FOMOD**

* Pre-build body mesh - yes
* ECE Slider - no
* CBPC ini file - Not install / Performance Imbroved/ balanced / **limited**
* Physics select - SMP and CBPC Lite / SMP and CBPC Lite2 / SMP and CBPC Heavy/ SMP and CBPC Heavy 2 / **Full SMP** / Only CBPC
* Boob physics - Very softness / Elasticity / **Realistic**
* Boob Colissoin select - **Small** / Medium / Big / Low(Small)
* Boobs Gravity - **Add Gravity** / Dont add Gravity
* Boobs push up - **Add Auto push up** / Don't add
* Belly Physics Preset - Very soft(Big bounce) / Very soft (Big bounce + weight) / Mascular(Small bounce) / **Mascular(Small bounce + Weight)**
* Butt Physics Preset - Very soft(Big bounce) / Very soft (Big bounce + weight) / Mascular(Small bounce) / **Mascular(Small bounce + Weight)**
* Leg Physics Preset - Very soft(Big bounce) / Very soft (Big bounce + weight) / Mascular(Small bounce) / **Mascular(Small bounce + Weight)**
* Change Vagina Colissoin - no
* Boobs physics preset - Option 1 / Optoin 2 / **Realistic**
* Movement - big / **medium** / small / a bit
* SOS - CBPC Regular / CBPC Floppy / **No SOS**
* Vagina Textures - Pink / Pale pink / red / dark red / **deep pink** / pale (choose 2k)
* Racemenu - None / **CBBE 3BA sliders**

**PATCHES**

* In FOMOD
- FlowerGirls

### [UNP](https://www.nexusmods.com/skyrimspecialedition/mods/1699)(NOT USING)

**NOTES**

* Body type - Pinup
* Options for HDT and TBBP physics but choose static for now
* Static Body options - nude
* 2k body diffuse mapsOption for ECE female head
* Option for SG Eyebrows only
* Options for SG texture renewal
* Option for UNP female body meshes
* If using Total Characer Makeover, overwrite that with this mod
* If using WICO use UNP version
* Skin Texture mods (Mature Skin, Fair Skin, etc) - Totally compatible but they will overwrite the mod's skin textures. Make sure it's for     UNP.
* Scars with pubic hair
* Normal Map - default
* Specular map - default between wet and default
* Subsurface map - dark red
* SG female eyebrows - no 


### [Tempered Skin for females](https://www.nexusmods.com/skyrimspecialedition/mods/8505)

**NOTES**: 

* Don't overwrite this mod to any other mod unless specifically asked
* CBBE or UNP versions available

**FOMOD**: 

* Body diffuse options - BO9
* Body normal options  - C4
* Face diffuse options - Younger/Elder/**Hard Life**
* Face normal options  - Smoother/**Rougher**
* Scars		      - check
* Tintmasks            - 2k
* Beast tintmasks      - 2k



### [Schlongs of Skyrim Lite](https://www.loverslab.com/files/file/3705-schlongs-of-skyrim-light-se/)(Not using)

**NOTES**:

* Manually place the .dll and .ini in the SKSE plugin folder

**FOMOD**:

* Body type - default
* Skin texture - hairy
* Schlong addons - Average Schlong 
* Skeleton - check
* [SOS](SOS) Shop - no

### [Schlongs of Skyrim Full](https://www.loverslab.com/files/file/5355-schlongs-of-skyrim-se/)

**NOTES**

* Body type - **Default** / BodyBuilder
* Skin Texture - **Hairless** / Hairy
* Schlong Type - **Reglar** / **Mascular** / **Average**
* Skeletons - yes

### [Tempered Skin for males](https://www.nexusmods.com/skyrimspecialedition/mods/7902)

**NOTES**: 

* If using SOS lite
- Download the SOS part 1 version
-  Download SOS part 2 version from [here](www.loverslab.com/files/file/3692-sos-light-version-with-tempered-skins-for-males-sse/), this is SOS light made to work with tempered skin for males
* If using SOS full
- Download SOS full version
* Let XP32 overwrite this
* Overwrite any NPC overhaul mods unless specifically asked not to


**FOMOD For SOS Full Version**

* Body Diffuse options -  Clean Hairless/ Clean light haired/ Clean Hairy/ Clean Bear/ Dirty Hairless/ Dirty light haired/ **Dirty hairy**/ dirty bear
* Body normal - Ripped/ **Natural** / Smooth Veins/ Smooth Sleek
* Face diffuse - Younger / Elder / **Hard Life** / Stubble face
* Face Normal - Smooth / **Harsh** / Weatherd
* Male/Beast tintmasks - 2k 

**FOMOD For SOS Lite Version Part 1**: 

* Body Type    - A2, BM adjusted 
* Body diffuse - dirty hairy
* Body normal  - natural
* Face Diffuse - hard life
* Face normal  - harsh
* Scars        - yes
* tintmasks    - 2k option for both


**FOMOD For SOS Lite version Part 2**

* Body DIffuse options - Clean Hairless/ Clean light haired/ Clean Hairy/ Clean Bear/ Dirty Hairless/ Dirty light haired/ **Dirty hairy**/ dirty bear
* Body normal - Ripped/**Natural**/ Smooth Veins/ Smooth Sleek
* Body shape - **Athlete**/ Athelete + JoP/ Atelete with Bigger Schlong/ Athelete with Bigger Schlong + Jop/ Body Builder/ Bodybulider with bigger schlong
* Schlong type - **Average**/ Mascular/ Regular
* Let other body mods overwrite this like Khajit or Argonian body mods

### [HIMBO](https://www.nexusmods.com/skyrimspecialedition/mods/46311)

**NOTES**

**FOMOD**

* MOrph Files - yes
* Physics - HDT-SMP with colissoins / HDT-SMP / **CBPC**
* Charmers of the Reach - No
* Refit - Yes
* Underwear -**Briefs** / Thong

**PATCHES**

* [Cathedral Armory](https://www.nexusmods.com/skyrimspecialedition/mods/67169)
* 


### [Norse God --HIMBO Bodyslide preset](https://www.nexusmods.com/skyrimspecialedition/mods/54296?tab=description)



### [Masculine Khajit Textures](https://www.nexusmods.com/skyrimspecialedition/mods/186?tab=description)

**NOTES**:

* Download the SOS lite version

**FOMOD**: 

* Variations - Grey Cat
* Chest textures - Furry

### [Feminine Khajit Textures](https://www.nexusmods.com/skyrimspecialedition/mods/183)

**NOTES**: 

* Download 2k versions
* Versions for Both UNP and CBBE available


**FOMOD**: 

* Variations - **Gray cat**/Leopard
* Chest variations - Abs/Normal Nipples/ Eight Nipples/ **Furry**

### [Male Dragonic Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/1443)(NOT USING)

**NOTES**:

* Possible issues with Shape Atlas for Men
* Download 2k SOS full

### [Female Dragonic Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/1442)(NOT USING)

**NOTES**: 

* Download 2K CBEE

### [Feminine Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/184?tab=files&file_id=229321&nmm=1)

**NOTES**:

* Download 2k Version
* Versoins for both UNP and CBBE available

**FOMOD**:

* Variations - Chameleon/**Lizard**
* Chest Variations - Abs/Bare Chest/ Heavy Scales/ Less Scales/ **Standard Scales**

### [Masculine Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/185)

**NOTES**

* Download the Better Males 2k version

**FOMOD**

* Variations - Lizard
* chest - Standard scales

### [Obody](https://www.nexusmods.com/skyrimspecialedition/mods/51084)

**NOTES**

* Download and install Obody
* Obody AE patch [here](https://www.nexusmods.com/Core/Libs/Common/Widgets/ModRequirementsPopUp?id=255638&game_id=1704&nmm=1)
* Download and install bodyslide
* Download as many presets as you want
* Make sure to have bodyslide patch for any armoour mod/retexture
* Make sure that any mods that change npc body are deselected
* make sure that obody is deselected
* Open Bodyslide
* Choose CBBE 3BA Amazing body
* check build morphs
* Click batch build
* choose build
* choose physics options
* aftear all's done close bodyslide studio
* Make a new mod from overwrite
* Re-enable deselected mods

## Misc


### [High Poly Vanilla hair](https://www.nexusmods.com/skyrimspecialedition/mods/41863)

**NOTES**: 

* High poly mesh replacer
* Compatible with re-texture mods

### [Vanilla Hair remake](https://www.nexusmods.com/skyrimspecialedition/mods/63979?tab=posts&BH=1)
**NOTES**

* Overwrite High poly vanilla hair if using
* Has physics support therefore requires [Faster HDT SMP](https://www.nexusmods.com/skyrimspecialedition/mods/57339?tab=videos#lg=2&slide=4)
* Replaces 7 vainlla hiar sptyles
* NPC file should load before any mod that edits NPCs

### [Superior Lore-Friendly hair](https://www.nexusmods.com/skyrim/mods/36510)

**NOTES**: 

* 2k Texture replacer for vanilla hair


### [Authentic Eyes](https://www.nexusmods.com/skyrimspecialedition/mods/36063)

**NOTES**:

* Not compatible with mods that change the vanilla eye textures
* Mods that change eye meshes may be compatible since this mod uses the vanilla meshes

### [Brows](https://www.nexusmods.com/skyrimspecialedition/mods/1062?tab=description)

**NOTES**: 

**FOMOD**:

* Full or vanilla replacer - full
* Resolution - High

### [Natural Teeth](https://www.nexusmods.com/skyrimspecialedition/mods/13765?tab=files)

**NOTES**

* Download the less yellow version

### [Expressive Facegen Morphs](https://www.nexusmods.com/skyrimspecialedition/mods/35785)

**NOTES**

* Requires Racemenu
* This mod is for players and won't efect NPC's
* The main file replaces Charagen and Race morphs, so it's not compatible with existing faces and presets.

### [Enhanced Blood textures](https://www.nexusmods.com/skyrimspecialedition/mods/2357?tab=files)

**NOTES**: 

* Download the standard install

**FOMOD**:

* SPID compatibility - Long distance(performance heavy script usage)
* Blood Size - Default splatter size
* Wounds  - EBT default
* Drips  - Defualt
* Screen Blood - default
* Res and Color - high res and defualt
* Alt blood textures - none

### [Equippable Underwear for NPC's](https://www.nexusmods.com/Core/Libs/Common/Widgets/ModRequirementsPopUp?id=255638&game_id=1704&nmm=1)
**NOTES**

* Not fully compatible with SOS-FULL
* Compatibile with SOS light
* Requires SPID
* Requires ConsoleUtilSSE

**PATCHES**

* [CBBE Bodyslide](https://www.nexusmods.com/skyrimspecialedition/mods/48121)


### [JS Purses and SEptimes SE](https://www.nexusmods.com/skyrimspecialedition/mods/30496)

**

## NPC Replacers


### [Cathedral Player and NPC's overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/24174)

**NOTES**

* Requires Expressive Facegen Morpsh
* Compatible with any mod that edits or adds NPC's but likely incompatilbe with other character visual overhauls


### [Bijin Warmaidens](https://www.nexusmods.com/skyrimspecialedition/mods/1825)

**NOTES**

* plugin should be preferably at the bottom
* Load ESP after EEO if using it
* All characters are compatible with Werewolf, Vampire Lord and Werebear forms with mods such as AFT.
* Let Tempered Skin of Males and Females overwrite this mod


**FOMOD**

* Options - **ALL-IN-ONE**/ Seperate Installation
* Delphine Face wrinkles - No Wrinkles/ **Wrinkles** 
* Body Type - UNP Body/ **CBBE Body**


### [Bijin NPC's SE](https://www.nexusmods.com/skyrimspecialedition/mods/11287)

**NOTES**

* Check the load order(Use LOOT or move their esp to the bottom manually).
* Check the load order between EEO(Ethereal Elven Overhaul) if you use it and Irileth face is having any problem.
* Carlottas weight has been changed
       
**FOMOD**: 

* Options - **ALL in One**/ Separate
* Adrianne Hairstyle Option - Hi babe/ **Vanilla based**
* Rikke face options - Non Wrinkles/ **Wrinkles**
* Hulda's face options - Non Wrinkles/ **Wrinkles**
* Maven's face options - Non Wrinkles/ **Wrinkles**
* Body Type - UNP/ **CBBE**

### [Bijin Wives](https://www.nexusmods.com/skyrimspecialedition/mods/11247)

**NOTES**:

* Check the load order(Use LOOT or move their esp to the bottom manually).
* Check the load order between EEO(Ethereal Elven Overhaul) if you use it and Irileth face is having any problem.

**FOMOD** 

* Options - **ALL in One**/ Separate
* Body Type - CBBE

### [Bijin Family - Salt and Wind Textures SE](https://www.nexusmods.com/skyrimspecialedition/mods/17083)

**NOTES**:

* Vanilla friendly hair for Bijin family

**FOMOD**:

* Chech Bijin wives, NPC's and Warmaidens

### [Bijin AIO SE](https://www.nexusmods.com/skyrimspecialedition/mods/11287?tab=files&file_id=30611&nmm=1)
**NOTES**

* USSEP compatibility
* Disable Bijin Warmaidens, Wives, and NPC's ESP

### [Project ja-Kha'jay- Khajiit Diversity Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/57610)

**NOTES**

* If using BeastHHBB, load Project la-Kha'jay after it
* All weapons are from Apex Khajiti Armoury
* Overwrite XPMSSE
* mods that edit the Khajiit race- you just need to forward the Hair Color data from this mod in any patch.
* custom race mods- NPCs will appear naked (or your custom race character will.) Needs a patch with xEdit. Here's how (NSFW site)
* NPC makeovers that have a BSA- either pack my mod's assets into a BSA and let overwrite, or unpack the other mod's BSA, let this mod overwrite
* mods that replace feet meshes like digitigrade and high poly meshes (like Tempered Skins for Males). They will mess up the textures. I’ve provided patches for Khajiit Overhaul, but I won’t personally make any for other mods.
* Khayla/Ri'saad don't look like their replacers- you'll need to forward the changes made by the replacer patches to overwrite any other patch data using xEdit.


**PATCHES**

* 2k Textures
* AI Overhaul
* Apex Khajiti Armory
* Better Claws and Gauntlets
* Better Claws and Gauntlets for Ohmes/ Ohmes-Raht
* Digitigrade_Khajit Argonia Raptor
* Expressive Facial Animations
* High Poly Skyrim-style M'aiq Replacer
* Imperious
* Khajit Overhaul
* Modular Clothing System
* Oblivion-style M'aiq Replacer
* Ohmes Ri'saad Replacer
* USSEP
* Varaharadero's Khayla Replacer
* Interesting NPCS
* Moonpath to Elsweyr
* Relationship Dialogue Overhaul

### [Complete NPC Overhaul - Argonians - High Poly Head](https://www.nexusmods.com/skyrimspecialedition/mods/57701?tab=description)
**NOTES**

* It covers all the argonians in the base game
* Works with any body retextures/remeshes
* Compatible with Finding Derkeethus
* Overwrite Cathedral NPC Overhaul

**FOMOD**

* Hairstyle - **Feathers** / Fins

**PATCHES**

* Argonian Finns
* Khash The Argonian
* Veezara Alive
* Flawn's Vanilla Argonians Redux
* CNPCO and Horns are forever
* in FOMOD
- USSEP
- AI Overhaul
- Cutting Room Floor
- AI Overhaul & Cutting Room Floor
- Save the Icerunner


### [Jiub - Morrowind Appearance](https://www.nexusmods.com/skyrimspecialedition/mods/18651)



### [TK Children](https://www.nexusmods.com/skyrimspecialedition/mods/5916)

**FOMOD**

* Language - English
* Features - USSEP Patch
* SS Glitch - None
* Babette eyes - Normal

**NOTES**: 

* Only needed *.tre files to be used with Simple Children
* Open the mod through explorer and delete everething except the mesh folder


### [Simple Children](https://www.nexusmods.com/skyrimspecialedition/mods/22789)

**NOTES**:

* Install update and overwrite it
* SimpleChildren.esp is an esm and esl flagged file. It should be at the top of your load order, anywhere after USSEP.
* FacegenForKids.esp is an esl flagged esp that should go fairly low in your order to overwrite any other edits to kids.
* Patches are all esl flagged esps, and can go anywhere after their respective mods except any that edit the vanilla children. Those should go below FacegenForKids.esp
* Mods that add new children are compatible
* Compatible with Wet and Cold
* If using immersive children SE, load immersive children after SimpleChildren.esp

**FOMOD**:

* Vanilla or USSEP - USSEP
* Texture choicec  - pale eye area

**PATCHES**:

* AI Overhaul
* Kidmer
* Populated Cities
* Patch for Babette glowing eyes
* AIO patch FOMOD:
                  1. Beyond Reach
	          2. Beyond Skyrim Bruma
		  3. Blackthorn
		  4. Cutting Room floor
		  5. Dragons Keep
	      	  6. EtAC
		  7. Falskaar
		  8. Gavrostead
		  9. Greater Skal village
		  10. Helgen reborn
		  11. Immersive weapon integration
		  12. Immersive world encounters
		  13. Books of Skyrim
		  14. Inconsequential NPC's
		  15. Interesting NPC's
		  16. Keld-Nar
		  17. Legacy of the Dragonborn
		  18. Moon and Star
		  19. Outlaws and Revolutionaries
	          20. Qaxe's Questorium
	          21. Rigmor of Bruma
	          22. Rigmor of Cyrodil
                  23. Settlements Expanded
	 	  24. Solitude Expansion
	          25. The forgotten City
	          26. The People of Skyrim Complete
		  27. Wyrmstooth
		  28. Adopt Avaentus Aretino options iwth USSEP and Prince and Pauper
		29. Skaal Coat addon
* Must have the USSEP updated patch [here](https://www.nexusmods.com/skyrimspecialedition/mods/39143)





# Animations

## Basic

### [NEMESIS PCEA]()

**NOTES**

* Using Nemesis with Halo Poser will cause CTD

### [NEMESIS](https://www.nexusmods.com/skyrimspecialedition/mods/60033)

**NOTES**

* Make sure this mod is installed before XPMSE
* The NEMESIS generator needs to be ran AFTER installing all the animation mods

### [Dynamic Animation Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/33746)

**NOTES**

* Requires Address Library
* Version available for AE, SE and VR

### [Animation Motion Revolution](https://www.nexusmods.com/skyrimspecialedition/mods/50258)

**NOTES**

* Compatible with any mod
* Aniiversery version and SE and VR version available

### [True Directional Movement](https://www.nexusmods.com/skyrimspecialedition/mods/51614)

**NOTES**

* Requires Address Library
* Requiries NEMESIS
* Requires SkyUI

### [Expressive Facial Animation -Male Edition-](https://www.nexusmods.com/skyrimspecialedition/mods/19532)
**NOTES**

* if the files are overwritten by other mods, it may cause problems such as clipping in eyebrows.
* This mod is not compatible with Female Facial Animation
* This mod is for vanilla head meshes and will not work properly with other meshes with different numbers of vertices or different order

### [Expressive Facial Animations - Female Edition](https://www.nexusmods.com/skyrimspecialedition/mods/19181)
**NOTES**

* Incompatible with Female Facial animations

### [Pristine Vanilla Movement](https://www.nexusmods.com/skyrimspecialedition/mods/66635)

**NOTES**

* Directional movement and SmoothCam are recommended


### [Eating Animations and Sounds](https://www.nexusmods.com/skyrimspecialedition/mods/42602)

**NOTES**

* Cannot be used with mods that animate when eating or drinking. For iNeed --Food Water and Sleep, please turn off the animation function in MCM.
* It is not compatible with mods that modify vanilla food and drink records. Create a patch with xEdit.
* If you want to use animation for the mod to add new food, add two Magic Effects to the effect column of the food you want to animate with CK. Set "aaaXXX_Animation_ME" to "Duration" in 5 seconds, then set "aaa_cooltimeTaberenai_ME" to "Duration" in 6 seconds. No settings such as "Magnitude" or "Conditions" are required. XXX is the name of the food. Please choose the one you like.
* This mod uses the folder numbers "1998"～"2203" and "11998"～"12203" of Dynamic Animation Replacer. Beware of folder number conflicts.
* This mod works by replacing ritualspell_charge.hkx (master spell animation) with an eating animation by DAR.
* So, if you have another mod's ritualspell_charge.hkx in your DAR, make its folder number smaller than this mod.
* Also, if Nemesis / FNIS (GenderSpecificAnimations, NemesisPCEA, etc.) changes the folder path of ritualspell_charge.hkx from vanilla, the same problem as above will occur. Exclude ritualspell_charge.hkx when using gender setting etc.
* SheatheWeaponOnly patch availalbe


**PATCHES**

* CACO
* INeed

**NOTES**

* Cannot be used with mods that animate when eating or drinking
* It is not compatible with mods that modify vanilla food and drink records. Create a patch with xEdit.
* This mod uses the folder numbers "1998"～"2203" and "11998"～"12203" of Dynamic Animation Replacer

### [Eating animations - My HD version - No double sounds - And patches SE](https://www.nexusmods.com/skyrimspecialedition/mods/44010)
**NOTES**

* Requieres above mod

**PATCHES**

* Drinking Fountains for Skyrim
* Aurlyn Danwstone
* Beyond Skyrim - Bruma
* Vilja in Skyrim
* HD Sweet roll vanilla replacer
* Unique Stros M'Kai Rum
* High Poly Project
* No Double Sounr

### [zxlice's ultimate potion animation - ZUPA](https://www.nexusmods.com/skyrimspecialedition/mods/45182)
**NOTES**

* This is an SKSE plugin
* It's script free
* Supports all potions mod
* DO NOT mash your F button while drinking, your weapon model will get stuck and need re equip or drink another potion to restore. 
* DO NOT resurrect alive NPCs who have potion in their backpack, they will play potion animation instantly after resurrect and their weapon will disappear 

### [HDT-SMP](https://www.nexusmods.com/skyrimspecialedition/mods/30872?tab=description)

**NOTES**

* 

## Player 


### [Sprint Swimming](https://www.nexusmods.com/skyrimspecialedition/mods/67028)

**NOTES**

* Requires DAR
* Requires powerofthree's Papyrus Extender

### [Wade in Watre](https://www.nexusmods.com/skyrimspecialedition/mods/42854)

**NOTES**

* REquires Bug Fixes SSE and SKSE
* Incompatible with Sink or Swim or TDG's Wade in Water

### [Conditional IDLE's](https://www.nexusmods.com/skyrimspecialedition/mods/34006)

**NOTES**

* Movmenent animations, compatible with True Directional Movement or 360 Movement behaviour
* Requires Address LIbarary and DAR
* Has a wade in water plugin availabe
* Compatible with EVG animation variance
* Compatible with True Directional Movement
* Compatible with Combat Gameplay overhaul
* compatible with 460 movement behaviour
* Compatible with ALLGUD
* Compatible with Simple Dual Sheath
* Compatible with cookeh's conditoinal and random idles
* Incompatible with Movement Behaviour Overhaul
* PCEA: Any animation that is the same 'type,' meaning run, walk or idle will ALWAYS have priority over mine. Disable any sets that include these.
* Anything that places the shield somewhere other than the hand: Hand will float over head for shield rain. I recommend ALLGUD or Simple Dual Sheath for adding more items onto your character.


## NPC


### [EVG Animation Variance](https://www.nexusmods.com/skyrimspecialedition/mods/38534?tab=description)

**NOTES**

* Requires DAR
* Compatible with NEMESIS
* Incompatible with Custom Racial Animation Path(NEEDS TWEAKING)
* PCEA animations will have priority over this
* Compaitleb with cooke'hs conditional and random idles
* Has player personality addon

**FOMOD**

* Install type - Recommended/ **Modular**
* Torch - **Full**/ Only 3rd person/ Replacer - Higher/ Replacer - Tilted	
* Repurposed Idles - no
* Personality Elder - yes(may cause crashes)
* Elder sitting - no
* Children - yes
* Young female - yes
* Imperial - yes
* Egoistic - yes
* Wounded - yes
* Smooth Floating - yes
* Laying Down - yes


### [Lively Children Animations](https://www.nexusmods.com/skyrimspecialedition/mods/67557)

**NOTES**

* Requires DAR

### [NPC Animation Remix](https://www.nexusmods.com/skyrimspecialedition/mods/63471)

* Fully compatible with EVG conditional idles and EVG Animation variance
* If using any replacer animation mods, simply put that mod below this mod in Mod order
* If don`t use mods like Simple Dual Sheath, install patch from optional files. Its files will overwrite shield patches from my other mods - it OK and intentional`

**PATCHES**

* EvG Animation variace special animation for elders patch

### [Conditional Tavern Cheerin](https://www.nexusmods.com/skyrimspecialedition/mods/63029)

**NOTES**

* Requires DAR

## Creatures

### [Animations from Skyrim Hores Renewal](https://www.nexusmods.com/skyrimspecialedition/mods/53102)

**NOTES**

* Requires Nemesis
* Incompatible with Combat Gameplay Overhaul
* Incompatible with FNIS Creature Pack

## NSFW

### [OSA](https://www.nexusmods.com/skyrimspecialedition/mods/17217?tab=description)

**NOTES**

* Put OSA very high(yes even before most of the essenstials)

**NOTES**

* Download version ['OSA-17217-2-023-SE'](https://www.nexusmods.com/skyrimspecialedition/mods/17217?tab=files&file_id=54568)
* Download only that because newer versions have been deliberately made incompatible

### [OStim](https://www.nexusmods.com/skyrimspecialedition/mods/40725)

**NOTES**

* Has versions for both AE and SE
* Also download [this](https://www.nexusmods.com/skyrimspecialedition/mods/61685?tab=files&file_id=256275&nmm=1) for AE

### [Osex](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=description)

**NOTES**

* Download version [OSex-17209-2-02SE-Alpha](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=files&file_id=54347)
* Note that when downloading animations packs from CEO, only download 2.02 versions, as updated versions will not work
* Animation packs to download are
- [Bad Boys of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=files&file_id=54350)
- [Bad Girls of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=files&file_id=54355)
- [Billys Animation pack for Ostim](https://www.nexusmods.com/skyrimspecialedition/mods/65444?tab=files)
- [Dual Wield](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=files&file_id=54357)
- [Wizard Sex](https://www.nexusmods.com/skyrimspecialedition/mods/17209?tab=files&file_id=54358)



### [Unofficial Osex Bug patch](https://www.nexusmods.com/skyrimspecialedition/mods/47584)

## Combat Animations

### [First Person Combat Animations Overhaul 2.0 -SIZE MATTERS](https://www.nexusmods.com/skyrimspecialedition/mods/45177)
**NOTES**

* Requires DAR
* Semi-compatible with Combat Gameplay Overhaul,
* Mod should be fine with NEMESIS/FNIS unless changed the same files in which case NEmesis files will take priority
* Compatible with Improved Camera
* Compatible with Finally first person Magic animations
* COmpatible with all 3rd person animation mods
* Not compatible with Joy of Perspective
* DAR priority numbers 777-785 are used



# Gameplay

## Survival

### [Campfire](https://www.nexusmods.com/skyrimspecialedition/mods/667)

**NOTES**

* Compatible with anything apparantly
* Let papyrusutil overwrite Campfire

**PATCHES**

* Better Dynamic Snow patch [here](https://www.nexusmods.com/skyrimspecialedition/mods/14415)

### [Last Seed](https://www.nexusmods.com/skyrimspecialedition/mods/56393)

**NOTES**

* Campfire required
* powerofthree's Papyrus Extender required
* Compatible with
- CACO
- Keep it Clean/ Bathing in Skyrim/ Dirt and Blood
- Time Flies
- Spell Wheel VR
- Cutting Room Floor
- Better Vampires

**PATCHES**

* Creation Club - Fishing
* Skyrim Fishing/ Hunting in Skyrim
* Water from wells
* Drinking Fountains of skyrim
* Hunterborn
* Skills of the Wild
* Beyond Skyrim - Bruma
* Apothecary
* Requiem
* Eating Animations and Sounds
* Animated Eating And sounds
* Your Own thoughts
* iWant Widgets
* Nordic UI

### [Last Seed - Water](https://www.nexusmods.com/skyrimspecialedition/mods/58338)

**NOTES**

* Doesn't need much patches since it uses powerofthree'spapyrus extender tweaks
* Version 1.14 pathc only 
* Optional more chance of finding dirty water option

**PATCHES**

* In FOMOD
- Water in Wells
- Fountains of Skyrim

### [Frostfall](https://www.nexusmods.com/skyrimspecialedition/mods/671)

**NOTES**

* Do not unpack any BSA arcihve
* Should be placed as high as possible
* Should be loaded before any mods that edit inns and taverns

### [Campfire and Frostfall - Unofficial SSE Update](https://www.nexusmods.com/skyrimspecialedition/mods/17925)
**NOTES**

* Requires Papyrus Util
* SSE Engine Fixes is required
* Compatible with Frostfall - Seasons
* This mod should overwrite Campfire

### [Dirt and Blood - Dynamic Visual Effects](https://www.nexusmods.com/skyrimspecialedition/mods/38886)
**NOTES**

* Requires Spell Perk Item Distributer
* Has very good compatibility
* is NOT compatible with mods that attempt to do the same in terms of dirt, as they will be redundant
* Let Dirt and Blood HD Retexture overwrite this mod

**PATCHES**

* Inn Soaps
* 

## Lewd

### [Excitable Subs](https://www.nexusmods.com/skyrimspecialedition/mods/53373)

**NOTES**

* If you want to run this alongside any Nexus version of BBLS you should disable OStim Voice Packs and OStim Tweaks in the BBLS MCM.  It is okay to leave BBLS Bed Woohoo and OStim Dialogues enabled in BBLS as they are compatible.

### [OCum](https://www.nexusmods.com/skyrimspecialedition/mods/45990/)

### [OEndurance](https://www.nexusmods.com/skyrimspecialedition/mods/61403)

### [OSearch](https://www.nexusmods.com/skyrimspecialedition/mods/43018)

### [OVoice](https://www.nexusmods.com/skyrimspecialedition/mods/53307)

### [OVirginity](https://www.nexusmods.com/skyrimspecialedition/mods/45563)

### [ORomance](https://www.nexusmods.com/skyrimspecialedition/mods/48564)


## Armor

### [Cloaks of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/6369?tab=files)

**NOTES**

* Cloaks are at slot 46

**FOMOD**

* Pick version - **Main**/ No Imperial/ Player Only

### [SKYBLIVION - 3E Cyrodiilic Steel Armor](https://www.nexusmods.com/skyrimspecialedition/mods/31206)

### [Imperial Guard Centurion Armor SE](https://www.nexusmods.com/skyrimspecialedition/mods/50410)

**NOTES**

* Notes 2k/4k version available
* ESL plugin available

### [Stormlord Armor](https://www.nexusmods.com/skyrimspecialedition/mods/7493?tab=files)

### [Bandolier - Bags and Pouches Classic](https://www.nexusmods.com/skyrimspecialedition/mods/2417)
**NOTES**

* Compatible with Winter is Coming
* Compatible with Leather Backpack
* Compatilbe with Saber Gear Backpack
* Compatible with Dwemer Goggles and Scouter
* Compatible with Wearable Landerns
* Has conflicts with with mods that use slots - {48, 55, 53, 57, 58, 52, 59}
* Has conflcts with Aesir Armor Mod
* Have conflicts with Sclongs Of Skyrim
* Has conflicts with Angelic Halos and Demonic Hornts

**PATCHES**

* [Schlongs of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/38538/?tab=description&BH=1) (has non-names update available) Also disable this mod when creating a bashed patch

### [Bandolier Bags and Pouches for NPC -Names update-](https://www.nexusmods.com/skyrimspecialedition/mods/19093)

**NOTES**

* ESL Flagged
* Script free
* Works great with mods adding NPC's 
  
**PATCHES**

* Cutting Room Floor
* Dawngard Arsenal
* Winter is Coming
* Cloaks of Skyrim
* Immersive Armors
* WACCF


## Weapons

### [Animated Armory](https://www.nexusmods.com/skyrimspecialedition/mods/35978)(NOT USING)

**NOTES**

* Requires DAR
* Animation Motion revolution is a soft requirement, it's used for katana power attacks
* Combat gameplay overhaul is semi incompatible
* SkySa is not compatible
* The DAR folders in use by this mod are 10, 11, 12, 13, 14, 16, 17, 18, 29, 30, 31 and 32 There is an included txt file explaining which folder is wha
* There is a fix for the mod that fixes tons of errors [here](https://www.nexusmods.com/skyrimspecialedition/mods/47213)
* It's compatible with Speed and Reach fix

**PATCHES**

* LoTD and Skyrim Unique Treasures
* Nikola's Amber and Madness
* Blue Frost Daedric Weapons
* SkyUI compatibllity patch
* Animated Armory Animation zPatch is a Zedit script that will run through your load order and add my mods. keywords to weapons it finds in your load order, this is great as it tailor makes compatibility patches for your load order, you wont need to download any compatibility patches after running this script
* First person Compat animatiosn overhaul - SIZE MATTERS
* Diamond Smithing Complete
* Vigilant
* Outlaws and Revolutionaries
* MERPs Gondorian Armament
* Relids of Hyrule
* T'Skyrim True Weapons 
* Lintra Spear
* Weapon balancing Script for Heavy Armory and Animated Armory for use in xEdit
* Animated Armoury - CC Dead Man's Dread patch
* Immersive Weapon Integration
* COOR
* Expanded Skyrim Weapons
* Heavy Armory
* Rapier and Dagger
* Immersive Weapons
* VAldacil's item sorting
* Patch for size matters [here](https://www.nexusmods.com/skyrimspecialedition/mods/46088)
  

### [Heavy Armory](https://www.nexusmods.com/skyrimspecialedition/mods/6308)(NOT USING)

**NOTES**

* Dowload the non-lite version
* Compatible with any texture replacer
* 

**PATCHES**

* aMidianborn Skyforge patch
* Animated Armory

### [Skyrim Spear Mechanic SE](https://www.nexusmods.com/skyrimspecialedition/mods/25146)

**NOTES**

* Requires SKSE, DAR, NIOverride
* Compatible with animated armory


## Combat

### [NPCs use Potions](https://www.nexusmods.com/skyrimspecialedition/mods/67489)

**NOTES**

* Compatible with zxlice's Ultimate option animation
* compatible with Potion Animated fix
* compatible with animation potion drinking SE
* Really compatible with anything
* Anniversery or AE versions available


**PATCHES**

* zxlices's Ulitmate Potion Animation

### [Smart NPC Potions](https://www.nexusmods.com/skyrimspecialedition/mods/40102)

**NOTES**

* This mod is recommended to be used with NPCs use Potions
* This mod is generally compatible with everything
* No spacial sound option available

### [Oxygen Meter 2](https://www.nexusmods.com/skyrimspecialedition/mods/64532)

**NOTES**

* Requires SKS64 & Address Library

### [SkySA](https://www.nexusmods.com/skyrimspecialedition/mods/50258)

**NOTES**

* Requires Animation motion revolution, DAR and Nemesis
* 

### [FP Revo First person combat overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/49050?tab=description)

**NOTES**

* Requires NEMESIS and DAR
* FOR CGO Users
- Delte 1hm_behaviour in: Nemesis_Enigne/mod/dscargo/_1stperson
- Run Nemesis
- Compatible with Size Matters

### [Incremental Injuries](https://www.nexusmods.com/skyrimspecialedition/mods/55114)

**NOTES**

* 

## Other

### [Simple Dual Sheath](https://www.nexusmods.com/skyrimspecialedition/mods/50049)

**NOTES**

* Not compatible with other dual sheath mods
* Change the ShieldonBack fag on \Data\SKSE\Plugin

### [Immrsive Equipment Displayes](https://www.nexusmods.com/skyrimspecialedition/mods/30496)

Allows you to display

### [Immersive Equipment Displays - ALLGUD Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/62449)
**NOTES**

* Potions are adjusted with Rustic Animated Potions in mind
* Alternative mesh tie is in ALLGUD which can be used by installing ALLGUD but deleting everything but the meshes
* Soulgems are adjusted with Rustic Soulgems in mind
* So use the preeset
- Go in game
- open the ui provided by IED
- Click on view
- Select custon
- Make suri the right gender is selcted
- Click on the preset bar and select the one you want to use
- Select Merge to add each preset to your current presets or Apply to replace all your presets by the one you selected 
- Choose the presets you want to keep and delete hte other ones
* If using a custom race, the race will needed to be added manually in the filters section of each item entries
* To addrace entries, go the filters section of the item. Then click "add one" and click on the next arrow to get "browse". From there, add your custom race. Repeat that to all filter entries

### [Rain and Snow and Ash Shaders](https://www.nexusmods.com/skyrimspecialedition/mods/22780)

**NOTES**

* By using it alongside Wet and Cold Gears - We get something superior than Wet and Cold

**FOMOD**

* Options - **ESL** / ESP (VR Vertions also available)

### [Wet and Cold - Gears](https://www.nexusmods.com/skyrimspecialedition/mods/54876?tab=description)
**NOTES**

* Requires the [original](https://www.nexusmods.com/skyrimspecialedition/mods/644) wet and cold.
* Install this mod and overwrite it with wet and cold
* Fur hoods fix [here](https://www.nexusmods.com/skyrimspecialedition/mods/11320)
* HD textures [here](https://www.nexusmods.com/skyrimspecialedition/mods/35539)

**FOMOD for Wet and Cold SE**

* Multi Monitor drips - no
* Children Hats - Mod compatibility meshes - no

### [CBPC Equipment physics](https://www.nexusmods.com/skyrimspecialedition/mods/58990?tab=description)
**NOTES**

* Requires XPMSSE
* Incompatible with CBP 
* If you're AE user, only required skeleton file so ignore requirements of XPMSSE like Racemenu and install i

**FOMOD**

* Sword - **Default** / Default -OnlyPlayer / Sword On Back / Sword On Back - OnlyPlayer / **Sword on Back (SWP)** / Sword On Back (SWP) - OnlyPlayer / **Sword on Left Hip** / Sword on Left Hip - OnlyPlayer
* Sword - **Default** / Default - OnlyPlayer / Sword On Back / Sword On Back - OnlyPlayer / Sword On Back(SWP) / Sword on back (SWP) - Only Player / **Sword Edge up** / Sword Edge up - OnlyPlayer / **Sword on Left Hip** / Sword on Left Hip - OnlyPlayer
* Shield - **Default** / Default - OnlyPlayer
* Mace - **Default** / Default - OnlyPlayer
* Mace(Left) - **Default** / Default - OnlyPlayer
* War Axes - **Default** / Default - Only Player / **Reversed Axe** / Reversed Axe - OnlyPlayer / **Axe on Back**  / Axe on Back - Only Player
* War Axes(Left) - **Default** / Default - OnlyPlayer - **Reversed Axe** / Reversed Axe - Only Player / **Axe on Back** / Axe on Back - OnlyPlayer
* Dagger - **Default** / Default - OnlyPlayer / **Dagger on Hip** / Dagegr on Hip - Only Player / Dagger on Ankle / Dagger on Ankle - Only player
* Dagger(Left) - **Default** / Default - OnlyPlayer / **Dagger on Hip** / Dagegr on Hip - Only Player / Dagger on Ankle / Dagger on Ankle - Only player
* GreatSword - **Default** / Default - OnlyPlayer / 2H on Back (SWP) / 2H on Back(SWP) - OnlyPlayer / **2H Edge Up at Waist** / 2H Edge up at Waist - OnlyPlayer
* BattleAxe and War Hammer - **Default** / Default - OnlyPlayer / 2H on Back (SWP) / 2H on Back(SWP) - OnlyPlayer / **2H Edge Up at Waist** / 2H Edge up at Waist - OnlyPlayer
* Bow - **Default** / Default - OnlyPlayer / Frostfall Bow / Frostfall Bow - OnlyPlayer / Reversed Bow / Reversed Bow - Only Player / Turn Bow / Turn Bow  - Only Player 
* Quiver - **Defualt** / Default - OnlyPlayer / Frostfall quiver / FrostFall quiver - OnlyPlayer / **FrostFall Quiver (XP32)** / Frostfall Quiver(XP32) Only player
* Crossbow - **Default** / Default - OnlyPlayer / Frostfall Crossbow / Frostfall Crossbow - OnlyPlayer
* Bold - **Default** / Default - OnlyPlayer / Frostfall / Frostfall - Only Player / Left Hip Bold Quiver / Left Hip Bold Quiver - Only Player / **Left Hip Bolt Quiver Alt** / Left Hip Bold Quiver Alt - Only Player / **Aesthetic bold Quiver** / Aesthetic Bolt Quiver - Only Player
* Staff - **Default** / Default - OnlyPlayer
* Staff(Left) - **Default** / Default - OnlyPlayer
* Equipment Physics - Loose / **Tight**

### [Alternate Conversation Camera Plus](https://www.nexusmods.com/skyrimspecialedition/mods/40722?tab=files)
### [Helps to Have a Map](https://www.nexusmods.com/skyrimspecialedition/mods/37238)

**NOTES**

* There is a batfile that needs to be put in the Skyrim Folder, not the data folder


# Audio

## Creature

### [Improved Horse Step Sounds](https://www.nexusmods.com/skyrimspecialedition/mods/848)

**NOTES**

* Is compatible with any other horse mod


# Quest and World expansions

## Small

## Large

### [Beyond Skyrim - Bruma](https://www.nexusmods.com/skyrimspecialedition/mods/10917?tab=description)
**NOTES**

* Download Main file
* Download Assets
* Download DLC Integration Patch
* Download Hotfix

