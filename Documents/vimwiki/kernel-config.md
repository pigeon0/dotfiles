
**GENERAL SETUP**


Kernel compression mode - zstd


POSIX Message qeues(=n) - Only needed for machines with Solaris


Enable process_vm_readv/writev_systemcalls(=y) - Don't know what it does, so leave default


uselib syscall(=n) - only needed for programs that use libc5 or earlier, since i'm going to use glibc, I'm going to keep this disabled


Auditing Support(=y) - needed for SELinux


Timer subsystem

 *Timer tick handling

   **Periotic timer(=y)

  *old idle dynticks config(=n)

  *high resolution timer support(=n)


Cpu Task time and stats accounting

*BSD Process accounting(=n)

*Export task/process statistics(=n) - could create harmless errors in htop



Kernel log buffer size(15)


CPU Kernel log buffer size(15)


Temporary CPU printk log buffer(12)


Initial Ram filesystems and Ramdisk(=y) - i need initramfs

*Disable all init compression except zstd


Compiler optimization level(-o2)


Optimize for size(=n)


Slab allocator

*SLUB



**PROCESSOR TYPE AND FEATURES**

Symmetric multi-processing support(=y)


MPS Table(=n)


Support for extended not PC x86 platforms(=n)


Processor Family

*Core2/newer xeon


Maximum number of CPUS(6)


Multi-core scheduler support(=n) - useful for only CPU's with larger number of threads.


Reroute for Broken boot IRQs(=n) - only useful for old CPU's


Intel MCE features(=y)


AMD MCE features (=n)


Performance monitoring

* Disable AMD featuers


IOPERM and IOPL emulation(=n) - only useful for legacy applications


CPU Microcode loading(=n)


Intel microcode loading options(=y)


AMD microcode loading options(=n)


Level 5 paging support(=n)


NUMA Memory Allocation and Scheduler support(=n)


Check for Low memory corruption(=y)


MTRR cleanup support(=y)

*MTRR Cleanup value(1)

*MTRR spare reg num(1)


Intel Memory protection keys(=y)


EFI Runtime service support(=y)


keexec system call(=y)


kernel crash dump(=y)


relocatable kernel(=y)


**POWER MANAGEMENT**

Suspend to RAM and standby(=n)


Hibernation(=n)


CPU Idle driver for intel processors(=y)


**ENABLE LOADABLE MODULE SUPPORT**(=y)

Forced module unloading(=n)

**ENABLE BLOCK LAYER**(=y)

Block Layer debugging information(=n)


Partition types

*Advanced partition selection(=y)
**EFI GUID Partition support(=y)
**MSDOS(=n)


**NETWORKING SUPPORT**(=y) - disable wireless support


**DEVICE DRIVERS**

PCCard support(=n)


IOMMU Hardware support

*AMD IOMMU(=n)
*Support for Shared Virtual Memory with Intel IOMMU(=y)
*nable Intel DMA Remapping Devices by default(=y)
*Support for Interrupt Remapping(=y)


VFIO non-priveleged driver framework

*VFIO no-IOMMU support
**GENERIC vfio support for any PCI devices(=m)
***generic vfio support for vga devices(=y)


FIRMWARE DEVICES

*Mark VGA/EFI....etc


SOUND CARD SUPPORT

*Advanced Linux Sound Archtecture
**PCI sound devices
***Intel/SIS...etc
**HD AUDIO
**USB Sound devices
***USB Audio MIDI Driver(=y)



Block devices(=y)

*Number of loop devices floating(0)


SCSI Device support

*Asynchronous SCSI Scannign(=y)


Multiple devices driver support(=n)


Macintosh device dirvers(=n)


Network devices

*Wireless LAN(=n)


INPUT DEVICE SUPPORT

*Tablets(=n)

*Touchscreen(=n)



Graphics support

*Maximum number of GPUS(2)
*AGP Support(=y) - maybe disable later
*Framebuffer devices
**support for framebuffer devices
***Simple frame buffer support(=y)


Character Devices

*IPMI Top-level message handler(=y)


**FILE SYSTEMS**

Miscellaneous file systems(=n)


Network file systems(=y)


**KERNEL HACKING**

RCU Debugging

*Stall timeout(3)


**GENTOO LINUX**

Support for init systems

*OpenRC(=y)
*SystemD(=n)
