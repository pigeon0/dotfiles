# Gentoo Installation 

## Preliminary

* Have any liveCD, preferrably one with GUI. Manjaro in this case.

## Disk Configuration

* Use fdisk
* Have root and home in seperate partitions
* Don't use btrfs together with LVM. Use btrfs in this case.

1. `parted /dev/sda`

- Type and enter 'g' to create new GUID Disk label
- Type and enter 'n' to create new partitions
- Keep the first sector default and choose partition size on 2nd sector, or keep it empty to use all free space.
- Press 't' to change partition type

2. Make 256M partition for EFI - /dev/sda1
3. Make 8G partition for swap, partition type should be linux swap - /dev/sda2
4. Make 50G partitoin for rooot, partition type should be linux filesystem - /dev/sda3
5. Use all free space to create partition for home, should also be linux filesystem - /dev/sda3

### Formatting Partitions

* `mkfs.vfat -F32 /dev/sda1`
* `mkfs.btrfs /dev/sda3`
* `mkfs.btrfs /dev/sda4`
* `mkswap /dev/sda2`

### Mounting

* `mkdir --parents /mnt/gentoo`
* `mkdir /mnt/gentoo/home`
* `mkdir /mnt/gentoo/boot`
* `mount /dev/sda3 /mnt/gentoo`
* `mount /dev/sda1 /mnt/gentoo/boot`
* `mount /dev/sda4 /mnt/gentoo/home`
* `swapon /dev/sd2`

## Stage 3

* Set date using `date 100313162021` where it's 03/10/2021 13:16
* `cd /mnt/gentoo`
* Downolad stage 3 tarball inside /mnt/gentoo using `wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20220605T170549Z/stage3-amd64-openrc-20220605T170549Z.tar.xz`
* extract using `tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner`

### Compilter options

* Download pre-set make.conf
* `cd /mnt/gentoo/etc/portage`
* `rm make.conf`
* `wget https://gitlab.com/pigeon0/dotfiles/-/blob/master/other/root/etc/portage/make.conf`

### Licenses

## Chrooting

### Ensuring compatibility for external liveCD's 

* `test -L /dev/shm && rm /dev/shm && mkdir /dev/shm`
* `mount --types tmpfs --options nosuid,nodev,noexec shm /dev/shm `
* `chmod 1777 /dev/shm /run/shm`

### Ebuild Repo

* `mkdir --parents /mnt/gentoo/etc/portage/repos.conf`
* `cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf`

### DNS Info
 
 * `cp --dereference /etc/resolv.conf /mnt/gentoo/etc/`

### Mounting

* `mount --types proc /proc /mnt/gentoo/proc`
* `mount --rbind /sys /mnt/gentoo/sys`
* `mount --make-rslave /mnt/gentoo/sys`
* `mount --rbind /dev /mnt/gentoo/dev`
* `mount --make-rslave /mnt/gentoo/dev`
* `mount --bind /run /mnt/gentoo/run `
* `mount --make-slave /mnt/gentoo/run`

### Entering the environment

* `chroot /mnt/gentoo /bin/bash`
* `source /etc/profile`
* `export PS1="(chroot) ${PS1}`

## Configuring Portage

* `emerge-webrsync`
* `emerge --ask mirrorselect`
* `mirrorselect -i -o >> /etc/portage/make.conf` and select Singapore
* `emerge --sync`
* `eselect profile list` choose the profile with SELinux, if it's 2 then;
* `eselect profile set 2`

### Cpu Flags

* `emerge --ask app-portage/cpuid2cpuflags`
* `cpuid2cpuflags`
* `echo "*/* $(cpuid2cpuflags)" > /etc/portage/package.use/00cpu-flags`

## Updating @world

* `emerge --ask --verbose --update --deep --newuse @world`
* `emerge --ask neovim`

## Timezone

* `echo "Asia/Dhaka" > /etc/timezone`
* `emerge --config sys-libs/timezone-data`

## Locale

* `nvim /etc/locale.gen`
```

```
* `locale-gen`
* `eselect locale list` - Select en_Us
* `env-update && source /etc/profile && export PS1="(chroot) ${PS1}"`


## Kernel Configuration

* `emerge --ask sys-kernel/linux-firmware`
* `eselect kernel list`
* `eselect kernel set 1`
* `emerge --ask sys-apps/pciutils`
* `cd /usr/src/linux`
* `wget https://gitlab.com/pigeon0/dotfiles/-/blob/master/other/usr/src/linux/.config`
* `make menuconfig`
* `emerge --ask app-arch/lz4`
* `make && make modules_install`

## Initramfs

* `emerge --ask sys-kernel/dracut`
* `dracut --kver=4.9.16-gentoo`

## Modules

* `find /lib/modules/<kernel version>/ -type f -iname '*.o' -or -iname '*.ko' | less`
* `mkdir -p /etc/modules-load.d `
* `nvim /etc/modules-load.d/[name]`

## fstab


* `wget https://gitlab.com/pigeon0/dotfiles/-/blob/master/other/etc/fstab`

## Networking

* `nvim /etc/conf.d/hostname`
* `nvim /etc/conf.d/net`

* `emerge --ask net-misc/dhcpcd`
* `rc-update add dhcpcd default `
* `rc-service dhcpcd start`

* `cd /etc/init.d`
* `ln -s net.lo net.eth0`
* `rc-update add net.eth0 default`

* `nvim /etc/hosts` - configure as per

```
# This defines the current system and must be set
127.0.0.1     tux.homenetwork tux localhost
  
# Optional definition of extra systems on the network
192.168.0.5   jenny.homenetwork jenny
192.168.0.6   benny.homenetwork benny

```

## System

* Set root password by `passwd`

### Keyboard layout

* `mkdir --parents /usr/share/keymaps/i386/mod-dh`
* `cd /usr/share/keymaps/i386/mod-dh`
* `wget https://github.com/ColemakMods/mod-dh/blob/master/console/colemak_dh_ansi_us.map`
* `nvim /etc/conf.d/keymaps` - edit as follows

```
keymap="us"
extended_keymaps="colemak_dh_ansi_us"
```

## Tools

### Logger

* `emerge --ask app-admin/sysklogd`
* `rc-update add sysklogd default`

### Cron Deamon

* `emerge --ask sys-process/cronie`
* `rc-update add cronie default`

### File Indexer

* `emerge --ask sys-apps/mlocate`

### OpenSSHD

* `rc-update add sshd default`

### Filesystem tools

* `emerge --ask sys-fs/e2fsprogs`
* `emerge --ask sys-fs/btrfs-progs`
* `emerge --ask sys-fs/dosfstools`


## Bootloader

`emerge --ask --verbose sys-boot/grub`

`grub-install --target=x86_64-efi --efi-directory=/boot`

if error - `Could not prepare Boot variable: Read-only file system`

do - `mount -o remount,rw /sys/firmware/efi/efivars`

`grub-mkconfig -o /boot/grub/grub.cfg`

* `exit`
* `cd`
* `umount -l /mnt/gentoo/dev{/shm,/pts,} `
* `umount -R /mnt/gentoo`
* reboot

## Finishing steps

### User

* `useradd -m -G users,wheel,audio -s /bin/bash orin`
* `passwd orin`

### Cleanup 

`rm /stage3-*.tar.*`

### 

* `FEATURES="-selinux" emerge -1 selinux-base`
* `FEATURES="-selinux -sesandbox" emerge -1 selinux-base`
* `FEATURES="-selinux -sesandbox" emerge -1 selinux-base-policy`
* `emerge -uDN @world`

* `mkdir /mnt/gentoo `
* `mount -o bind / /mnt/gentoo`
* `setfiles -r /mnt/gentoo /etc/selinux/strict/contexts/files/file_contexts /mnt/gentoo/{home} `
* `umount /mnt/gentoo`
* `semanage fcontext -a -t swapfile_t "/dev/sda2"`
* `restorecon /dev/sda2`
* `rlpkg -a -r`
* `rlpkg screen`
