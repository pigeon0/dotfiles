#!/bin/bash

ICON1=
ICON2=

NETSTAT=$(ping -c 1 -q google.com >&/dev/null; echo $?)

if [ $NETSTAT -eq 0 ]
then
	echo "$ICON1"
else
	echo "$ICON2"
fi
