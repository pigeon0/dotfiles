# My Dotfiles

This contains all the Dotfiles for my Gentoo rice

# DWM

## Patches

[Always Center](https://dwm.suckless.org/patches/alwayscenter/)   [Steam](https://dwm.suckless.org/patches/steam/)       [Winicon](https://dwm.suckless.org/patches/winicon/) 

[Pertag](https://dwm.suckless.org/patches/pertag/)          [Underline Tags](https://dwm.suckless.org/patches/underlinetags/)     [Fullgaps](https://dwm.suckless.org/patches/fullgaps/)

# Bar

Modified dwmblocks fork of [Ashish Yadav](https://github.com/ashish-yadav11/dwmblocks)

# Compositor

Modified [Jonaburg-picom-fix](https://github.com/Arian8j2/picom-jonaburg-fix)

# <center>Screenshots</center>
<p align="center">
  <img width="49%" src="https://gitlab.com/pigeon0/dotfiles/-/raw/main/screenshot/2022-02-13-01:47:36-screenshot.png" />
  <img width="49%" src="https://gitlab.com/pigeon0/dotfiles/-/raw/main/screenshot/2022-02-13-01:45:56-screenshot.png" />
  <img width="49%" src="https://gitlab.com/pigeon0/dotfiles/-/raw/main/screenshot/2022-02-13-01:46:49-screenshot.png" />
  <img width="49%" src="https://gitlab.com/pigeon0/dotfiles/-/raw/main/screenshot/2022-02-13-01:37:02-screenshot.png" />
</p>
